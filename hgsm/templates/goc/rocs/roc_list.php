<?php
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH."formWebObj.php";

/**
 * @package goc
 */
class roc_list extends WebObject
{
  function init()
    {
      $this->addSVar('mode', 'view');  // view | edit
    }

  function on_set_mode($event_args)
    {
      $mode = $event_args['mode'];

      //(double)check that only a GOC admin can go to edit mode
      if ($mode=='edit' and !is_goc_admin())  return; 

      if ($mode=='view')
        {
          WebApp::setSVar('roc_edit->mode', 'hidden');
        }

      $this->setSVar('mode', $mode);
    }

  function on_del($event_args)
    {
      include_once TPL.'/goc/delete.php';
      $roc_id = $event_args['ROC_id'];
      $roc_list = "'$roc_id'";

      //delete the ROC and its countries
      del_rocs($roc_list);

      //update the menu
      include_once MENU.'/rebuild_menu.php'; 
    }

  function on_add($event_args)
    {
      WebApp::setSVar('roc_edit->mode', 'add');
      WebApp::setSVar('roc_edit->ROC_id', UNDEFINED);
    }

  function on_edit($event_args)
    {
      WebApp::setSVar('roc_edit->mode', 'edit');
      WebApp::setSVar('roc_edit->ROC_id', $event_args['ROC_id']);
    } 
}
?>