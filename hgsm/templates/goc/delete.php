<?php
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This file contains functions that are used for deleting a ROC,
 * country or site. For a site, the related contacts, downtimes etc.
 * that are related to this site are deleted as well. For a country,
 * the sites that are related to it are deleted as well. Similarly,
 * all the countries that are related to a ROC are deleted as well,
 * when the ROC is deleted.
 */

/**
 * $site_list is a comma-separated list of site ids. All the sites 
 * in the list are deleted. For each of the sites in the list, 
 * the related contacts, downtimes etc. that are related 
 * to this site are deleted as well.
 */
function del_sites($site_list)
{
  //delete the related site_contacts
  $query = "DELETE FROM site_contacts WHERE site_id IN ($site_list)";
  WebApp::sqlExec($query);

  //delete the related site_downtimes
  $query = "DELETE FROM site_downtimes WHERE site_id IN ($site_list)";
  WebApp::sqlExec($query);

  //delete the related site_nodes
  $query = "DELETE FROM site_nodes WHERE site_id IN ($site_list)";
  WebApp::sqlExec($query);

  //delete the sites in the list
  $query = "DELETE FROM sites WHERE site_id IN ($site_list)";
  WebApp::sqlExec($query);
}

/**
 * Delete all the countries in the list, and all the sites related to them.
 * $country_list is a comma-separated list of country_id-s.
 */
function del_countries($country_list)
{
  //get a list of the sites in each country
  $query = "SELECT site_id FROM sites WHERE country_id IN ($country_list)";
  $rs = WebApp::sqlQuery($query);
  $arr_sites = $rs->getColumn('site_id');
  $site_list = "'" . implode("', '", $arr_sites). "'";

  //delete the related sites
  del_sites($site_list);

  //delete the countries in the list
  $query = "DELETE FROM countries WHERE country_id IN ($country_list)";
  WebApp::sqlQuery($query);
}

/**
 * Delete all the ROCs in the list and all the countries related to them.
 * $roc_list is a comma-separated list of ROC_id-s.
 */
function del_rocs($roc_list)
{
  //get a list of the countries in each ROC
  $query = "SELECT country_id FROM countries WHERE ROC_id IN ($roc_list)";
  $rs = WebApp::sqlQuery($query);
  $arr_countries = $rs->getColumn('country_id');
  $country_list = "'" . implode("', '", $arr_countries). "'";

  //delete the related countries
  del_countries($country_list);

  //delete the ROCs in the list
  $query = "DELETE FROM ROCs WHERE ROC_id IN ($roc_list)";
  WebApp::sqlExec($query);
}
?>