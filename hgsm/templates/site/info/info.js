// -*-C-*- //tell emacs to use C mode
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function change_id()
{
  var form = document.site_edit;
  var site_id = form.site_id.value;

  SendEvent('info', 'change_id', 'site_id='+site_id);
}

function set_admin()
{
  var form = document.site_edit;
  var admin = form.admin.value;

  SendEvent('info', 'set_admin', 'admin='+admin);
}

function save_site()
{
  var form = document.site_edit;
  if (!validate(form))  return;
  var event_args = getEventArgs(form);
  SendEvent('info', 'save', event_args);
}

function validate(form)
{
  return true;
}
