// -*-C-*- //tell emacs to use C mode
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function add_node()
{
  SendEvent('node_list', 'add');
}

function edit_node(node_id)
{
  SendEvent('node_list', 'edit', 'node_id='+node_id);
}

function del_node(node_id)
{
  SendEvent('node_list', 'del', 'node_id='+node_id);
}
