<?php
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package site
 */
class node_edit extends formWebObj
{
  var $node_record = array(
                           'type'         => '',
                           'machine_name' => '',
                           'arch'         => '',
                           'os'           => '',
                           'cpu'          => '',
                           'ram'          => '',
                           'storage'      => '',
                           'middleware'   => '',
                           'comments'     => ''
                           );
  
  function init()
    {
      $this->addSVar('mode', 'hidden');  // add | edit | hidden
      $this->addSVar('node_id', UNDEFINED);
    }

  function on_save($event_args)
    {
      //(double)check that only a site admin can save changes
      $site_id = WebApp::getSVar('site->id');
      if (!is_site_admin($site_id))
        {
          WebApp::message(T_("Only a site admin can do this!"));
          return;
        }

      $mode = $this->getSVar('mode');
      
      if ($mode=='add')
        {
          //add the new node
          WebApp::execDBCmd('add_node', $event_args);

          //set the new id
          $this->setSVar('node_id', $event_args['node_id']);
        }
      elseif ($mode=='edit')
        {
          WebApp::execDBCmd('update_node', $event_args);
        }

      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          WebApp::addVars($this->node_record);
        }
      elseif ($mode=='edit')
        {
          $rs = WebApp::openRS('get_node');
          WebApp::addVars($rs->Fields());
        }
    }
}
?>