<?php
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package main
 */
class main extends WebObject
{
  function init()
    {
      WebApp::addSVar('interface', 'goc');
      WebApp::addSVar('interface_file', 'goc/goc.html');
      WebApp::addSVar('language', 'en');
      WebApp::addSVar('codeset', 'iso-8859-1');
    }

  function on_select($event_args)
    {
      $interface = $event_args['interface'];
      switch ($interface)
        {
        default:
        case 'goc':
          $interface_file = 'goc/goc.html';
          break;
        case 'roc':
          $interface_file = 'roc/roc.html';
          $roc_id = $event_args['roc_id'];
          WebApp::setSVar('roc->id', $roc_id);
          WebApp::setSVar('roc->mode', 'view');
          break;
        case 'country':
          $interface_file = 'country/country.html';
          $country_id = $event_args['country_id'];
          WebApp::setSVar('country->id', $country_id);
          WebApp::setSVar('country->mode', 'view');
          break;
        case 'site':
          $interface_file = 'site/site.html';
          $site_id = $event_args['site_id'];
          WebApp::setSVar('site->id', $site_id);
          WebApp::setSVar('site->mode', 'view');
          break;
        }
      WebApp::setSVar('interface', $interface);
      WebApp::setSVar('interface_file', $interface_file);
    }

  function on_language($event_args)
    {
      //set the selected language
      $lng = $event_args['lng'];
      WebApp::setSVar('language', $lng);

      //set the corresponding codeset
      $rs = WebApp::openRS('get-charset');
      $charset = $rs->Field('charset');
      WebApp::setSVar('codeset', $charset);
    }

  function onParse()
    {
      $lng = WebApp::getSVar('language');
      $codeset = WebApp::getSVar('codeset');
      global $l10n;
      $l10n->set_lng($lng, $codeset);
    }

  function onRender()
    {
    }
}
?>