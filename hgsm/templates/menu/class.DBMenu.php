<?php
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once dirname(__FILE__)."/class.MenuItem.php";

/**
 * Read the ROCs, countries and sites from the DB and build a menu.
 * @package menu
 */ 
class DBMenu
{
  var $root;             //root element of the menu

  function DBMenu()
    {
      $this->root = new MenuRoot;
      $goc = new MenuItem('goc', 'GOC DB', "select_goc()");
      $this->add_rocs(&$goc);
      $this->root->add_child(&$goc);
    }

  function add_rocs($goc)
    {
      $rs = WebApp::sqlQuery('SELECT ROC_id FROM ROCs');
      while (!$rs->EOF())
        {
          $id = $rs->Field('ROC_id');

          $roc = new MenuItem($id, $id, "select_roc(\'$id\')");
          $this->add_countries(&$roc);
          $goc->add_child(&$roc);

          $rs->MoveNext();
        }
    }

  function add_countries($roc)
    {
      $roc_id = $roc->id;
      $query = "SELECT country_id, name FROM countries WHERE ROC_id='$roc_id'";
      $rs = WebApp::sqlQuery($query);

      while (!$rs->EOF())
        {
          $id = $rs->Field('country_id');
          $name = $rs->Field('name');

          $country = new MenuItem($id, $name, "select_country(\'$id\')");
          $this->add_sites(&$country);
          $roc->add_child(&$country);

          $rs->MoveNext();
        }
    }

  function add_sites($country)
    {
      $c_id = $country->id;
      $query = "SELECT site_id FROM sites WHERE country_id='$c_id'";
      $rs = WebApp::sqlQuery($query);

      while (!$rs->EOF())
        {
          $id = $rs->Field('site_id');

          $site = new MenuItem($id, $id, "select_site(\'$id\')");
          $country->add_child(&$site);

          $rs->MoveNext();
        }
    }
}
?>