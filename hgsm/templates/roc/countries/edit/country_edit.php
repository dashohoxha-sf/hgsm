<?php
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package roc
 */
class country_edit extends formWebObj
{
  var $country_record = array(
                              'country_id' => '',
                              'name'       => '',
                              'email'      => '',
                              'phone'      => '',
                              'admin'      => ''
                              );
  
  function init()
    {
      $this->addSVar('mode', 'hidden');  // add | edit | hidden
      $this->addSVar('country_id', UNDEFINED);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //check that country_id does not exist in the table
          $rs = WebApp::openRS('get_country', $event_args);
          if (!$rs->EOF())
            {
              $this->country_record = $event_args;
              $country_id = $event_args['country_id'];
              $msg = "Country ID 'v_country_id' is used for another country";
              $msg = str_replace('v_country_id', $country_id, $msg);
              WebApp::message($msg);
              return;
            }

          //add the new country
          $event_args['timestamp'] = time();
          WebApp::execDBCmd('add_country', $event_args);

          //set the new id
          $this->setSVar('country_id', $event_args['country_id']);

          //rebuild the menu items
          include_once MENU.'/rebuild_menu.php'; 
        }
      else if ($mode=='edit')
        {
          WebApp::execDBCmd('update_country', $event_args);
        }

      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          WebApp::addVars($this->country_record);
        }
      else if ($mode=='edit')
        {
          $args = array('country_id' => $this->getSVar('country_id'));
          $rs = WebApp::openRS('get_country', $args);
          $vars = $rs->Fields();
          WebApp::addVars($vars);
        }
    }
}
?>