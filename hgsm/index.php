<?php
/*
Copyright 2001,2002,2003 Dashamir Hoxha, dashohoxha@users.sourceforge.net

This file is part of phpWebApp.

phpWebApp is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

phpWebApp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with phpWebApp; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  
*/

/**
 * Standard file for all the applications,
 * doesn't need to be modified.
 * @package phpwebapp
 */

//get the variables from the part of url after the question mark (?)
//it is used to jump directly (cross reference) to a certain node
//of the tree (roc, country or site)
//the format of the query string is like this:
// index.php?roc=SEE-GRID or index.php?country=AL or index.php?site=INIMA
if ($_SERVER["QUERY_STRING"]!='')
{
  if     (isset($_GET['site']))     $interface = 'site';
  elseif (isset($_GET['country']))  $interface = 'country';
  elseif (isset($_GET['roc']))      $interface = 'roc';
  else                              $interface = 'goc';
  $id = $_GET[$interface];

  //build the event string
  $event_args = "interface=$interface;${interface}_id=$id";
  $strEvent = "event=main.select($event_args)";
}

include_once "webapp.php";
include_once "authenticate.php";

//construct the target page of the transition
$tpl_page = TPL.$event->targetPage;
WebApp::constructHtmlPage($tpl_page);

//if there was a query string, redirect the page to the specified node
//the second parameter of GoTo() tells it to open
//'index.php' next, instead of 'xref.php' (this file).
if ($_SERVER["QUERY_STRING"]!='')
{
  print "<script language='javascript'>
  GoTo('main/main.html?$strEvent', 'index');
</script>";
}
?>
