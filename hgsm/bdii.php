<?php
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This script generated a list of BDII URLs of the sites
 * Usage: bdii.php?type1+type2+type3
 * where site type can be: production | pre_production | experimental
 */

$site_types = 'any';
if ($_SERVER["QUERY_STRING"]!='')  $site_types = $_SERVER["QUERY_STRING"];

include_once "webapp.php";

print "<xmp>\n";
print "\n### sites=$site_types";
print "\n### generated on ".date('Y-m-d H:i:s');
print "\n###----------------------------------------------------\n\n";
print_sites($site_types);
print "</xmp>\n";

function print_sites($site_types)
{
  if ($site_types=='any')
    {
      $query = "SELECT country_id, site_id, giis_url FROM sites";
      process($query);
    }
  else
    {
      $arr_types = explode('+', $site_types);
      for ($i=0; $i < sizeof($arr_types); $i++)
        {
          $type = $arr_types[$i];
          print "\n### $type sites \n";
          $query = ("SELECT country_id, site_id, giis_url "
                    . "FROM sites "
                    . "WHERE type='$type'");
          process($query);
        }
    }
}

/** execute the given query and output each record of the result */
function process($query)
{
  $rs = WebApp::sqlQuery($query);
  while (!$rs->EOF())
    {
      extract($rs->Fields());
      print "\n# $country_id/$site_id\n$site_id $giis_url\n";
      $rs->MoveNext();
    }
}

?>