#!/bin/bash
### update old *.po files with new messages extracted by xgettext.sh

### go to this directory
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 ll_CC"
  echo "where ll_CC is the language code, like en_US or sq_AL"
  exit 1
fi

lng=$1

### convert the *.po file of the application
app_name=$(./get_app_name.sh)
dir=$lng/LC_MESSAGES
mv $dir/$app_name.po $app_name-$lng-old.po
msgmerge --output-file=$dir/$app_name.po \
         $app_name-$lng-old.po  $app_name.po
