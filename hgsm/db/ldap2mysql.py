#! /usr/bin/env python
"""Updates site database with data from the information system."""

# siteDB connect info
HOST="host.domain"
USER="update"
PASS="upassword"
DB="gocdb"

import sys
import time
import ldap # SL3: python-ldap-2.0.7 from sf.net seems to work
import MySQLdb

# output controls - by default only failures are printed
quiet = 0   # don't print failed operations
verbose = 0 # print successfull operations

# output helpers
def log(msg):
    """Logs the current time and message on stdout."""
    print time.asctime(), msg

def info(msg):
    """Logs the message only in verbose mode."""
    if verbose: log(msg)

def warning(msg):
    """Logs the message unless in quiet mode."""
    if not quiet: log(msg)

def error(msg):
    """Logs the message and exits."""
    log(msg)
    sys.exit(1)

def openDB():
    """Connects to the database."""
    try:
        db = MySQLdb.connect(host=HOST, user=USER, passwd=PASS, db=DB)
        info("Connected to MySQL %s@%s:%s" % (USER, HOST, DB))
        return db
    except MySQLdb.MySQLError, e:
        error("MySQL error: %s" % e)

def getSites(db):
    """Returns list of (site, url) tuples from db.sites."""
    try:
        c = db.cursor()
        c.execute("SELECT site_id, giis_url FROM sites")
        return c.fetchall()
    except MySQLdb.MySQLError, e:
        error("MySQL error: %s" % e)

def getInfo(site, url):
    """Returns site LDAP data from url."""
    info("%s: Fetching '%s'" % (site, url))
    try:
        try:
            split = url.rindex("/")
        except ValueError:
            warning("%s: Bad URL: '%s'" % (site, url))
            return

        host = url[:split]
        base = url[split+1:]

        l = ldap.initialize(host)
        # prevents long hangs on firewall
        l.set_option(ldap.OPT_NETWORK_TIMEOUT, 10)
        l.simple_bind_s("", "")
        return l.search_s(base, ldap.SCOPE_SUBTREE)

    except ldap.LDAPError, e:
        warning("%s: LDAP error: %s" % (site, e[0]["desc"]))

def runSQL(db, sql):
    """Runs an SQL command over the given database connection."""
    try:
        info("%s: Running '%s'" % (site, sql))
        db.cursor().execute(sql)
    except MySQLdb.MySQLError, e:
        warning("%s: Command failed '%s': %s" % (site, sql, e))

def esc(str):
    """Escapes string before inserting it in a SQL command."""
    return MySQLdb.escape_string(str)

def getValues(entries, attr):
    """Returns all values for attr from the first entry containing the attr."""
    # e[0] is dn, e[1] is av pairs
    val = [e[1][attr] for e in entries if attr in e[1]]

    if len(val) > 0:
        return val[0]   # values from first entry
    else:
        return []

def getValue(entries, attr):
    """Returns first value for attr from the first entry containing the attr."""
    val = getValues(entries, attr)

    if len(val) > 0:
        return val[0]   # first value for attr
    else:
        return None

def runUpdates(db, site, entries):
    """Finds and runs all update* functions in this module."""

    # no point in doing anything if there is no data available
    if entries == None:
        return

    # find and run update* functions
    [obj((db, site, entries)) for (name, obj) in globals().items()
                if callable(obj) and name.find("update") == 0 ]

def updateCoords((db, site, entries)):
    """Updates longitude/latitude for site (used for maps)"""
    lat = getValue(entries, "GlueSiteLatitude")
    lon = getValue(entries, "GlueSiteLongitude")

    if lat or not lon: return

    runSQL(db,
        "UPDATE sites SET latitude=%s, longitude=%s WHERE site_id='%s'" %
            (esc(lat), esc(lon), esc(site)))

def updateLcgVer((db, site, entries)):
    """Updates LCG version for site/nodes."""
    apps = getValues(entries, "GlueHostApplicationSoftwareRunTimeEnvironment")

    if not apps: return

    # find the highest version
    lcgs = [a for a in apps if a.find("LCG-") == 0]
    lcgs.sort()
    lcg = lcgs.pop()

    runSQL(db, "UPDATE sites SET middleware='%s' WHERE site_id='%s'" %
        (esc(lcg), esc(site)))
    runSQL(db, "UPDATE site_nodes SET middleware='%s' WHERE site_id='%s'" %
        (esc(lcg), esc(site)))

def updateBatch((db, site, entries)):
    """Updates batch system name/ver."""
    batch = getValue(entries, "GlueCEInfoLRMSVersion")

    if not batch: return

    runSQL(db, "UPDATE sites SET batch_system='%s' WHERE site_id='%s'" %
        (esc(batch), esc(site)))

def updateOS((db, site, entries)):
    """Updates os name/ver."""
    os = getValue(entries, "GlueHostOperatingSystemName")
    ver = getValue(entries, "GlueHostOperatingSystemRelease")

    if not os or not ver: return

    runSQL(db, "UPDATE sites SET OS='%s %s' WHERE site_id='%s'" %
        (esc(os), esc(ver), esc(site)))

def updateHomepage((db, site, entries)):
    """Updates site homepage."""
    homepage = getValue(entries, "GlueSiteWeb")

    if not homepage: return

    runSQL(db, "UPDATE sites SET homepage='%s' WHERE site_id='%s'" %
        (esc(homepage), esc(site)))

def updateStorageSize((db, site, entries)):
    """Updates storage size."""
    avail = getValue(entries, "GlueSAStateAvailableSpace")
    used = getValue(entries, "GlueSAStateUsedSpace")

    if not used or not avail: return

    # sizes are in kb
    total = (int(avail) + int(used))/1024/1024

    runSQL(db, "UPDATE sites SET storage_size=%s WHERE site_id='%s'" %
        (total, esc(site)))

def updateEmails((db, site, entries)):
    """Updates site emails."""
    mail = getValue(entries, "GlueSiteSysAdminContact")
    if mail:
        mail = mail.replace("mailto: ", "");
        runSQL(db, "UPDATE sites SET email='%s' WHERE site_id='%s'" %
            (esc(mail), esc(site)))

    smail = getValue(entries, "GlueSiteSecurityContact")
    if smail:
        smail = smail.replace("mailto:", "");
        runSQL(db, "UPDATE sites SET csirt_email='%s' WHERE site_id='%s'" %
            (esc(smail), esc(site)))

def updateVOs((db, site, entries)):
    """Updates list of supported VOs."""
    queues = getValues(entries, "GlueClusterService")

    if not queues: return

    vos = ", ".join([q[q.rindex("-")+1:] for q in queues])
    runSQL(db, "UPDATE sites SET VOs_supported='%s' WHERE site_id='%s'" %
        (esc(vos), esc(site)))

def updateApps((db, site, entries)):
    """Updates list of installed applications."""
    apps = getValues(entries, "GlueHostApplicationSoftwareRunTimeEnvironment")

    if not apps: return

    # format is VO-vo-app-ver
    vo_apps = ", ".join([a.replace("VO-", "") for a in apps
                            if a.find("VO-") == 0])

    runSQL(db, "UPDATE sites SET apps_supported='%s' WHERE site_id='%s'" %
        (esc(vo_apps), esc(site)))

if __name__ == "__main__":
    # parse cmd line
    for opt in sys.argv[1:]:
        if opt == "-v":
            verbose = 1
        elif opt == "-q":
            quiet = 1

    db = openDB()
    for site, url in getSites(db):
        # if site == "HR-01-RBI":
        # if site == "AEGIS01-PHY-SCL":
            runUpdates(db, site, getInfo(site, url))

# vim:ts=4:sw=4:et
