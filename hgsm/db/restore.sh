#!/bin/bash

### go to this directory
cd $(dirname $0)

### get the DB access parameters
. dbaccess.sh

### restore the database 'hgsm' from the backup file
mysql --host=$HOST --user=$USER --password=$PASSWD \
      --database=$DBNAME < hgsm.sql
