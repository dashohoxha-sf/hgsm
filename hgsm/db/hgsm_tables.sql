-- MySQL dump 8.23
--
-- Host: 127.0.0.1    Database: hgsm
---------------------------------------------------------
-- Server version	3.23.58

--
-- Current Database: hgsm
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ hgsm;

USE hgsm;

--
-- Table structure for table `ROCs`
--

DROP TABLE IF EXISTS ROCs;
CREATE TABLE ROCs (
  ROC_id varchar(10) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  admin text NOT NULL,
  email varchar(255) NOT NULL default '',
  phone varchar(255) NOT NULL default '',
  comments text,
  timestamp varchar(255) NOT NULL default '',
  PRIMARY KEY  (ROC_id)
) TYPE=MyISAM;

--
-- Table structure for table `VOs`
--

DROP TABLE IF EXISTS VOs;
CREATE TABLE VOs (
  VO_id varchar(10) NOT NULL default '',
  name varchar(30) default NULL,
  PRIMARY KEY  (VO_id)
) TYPE=MyISAM;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS countries;
CREATE TABLE countries (
  country_id varchar(5) NOT NULL default '',
  ROC_id varchar(10) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  admin text NOT NULL,
  email varchar(255) NOT NULL default '',
  phone varchar(255) NOT NULL default '',
  comments text,
  timestamp varchar(255) NOT NULL default '',
  PRIMARY KEY  (country_id)
) TYPE=MyISAM;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS languages;
CREATE TABLE languages (
  id varchar(10) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  charset varchar(100) NOT NULL default '',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS session;
CREATE TABLE session (
  id varchar(255) NOT NULL default '',
  vars text NOT NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Table structure for table `site_contacts`
--

DROP TABLE IF EXISTS site_contacts;
CREATE TABLE site_contacts (
  contact_id int(10) unsigned NOT NULL auto_increment,
  site_id varchar(20) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  email varchar(30) NOT NULL default '',
  phone varchar(30) NOT NULL default '',
  role varchar(100) NOT NULL default '',
  hours varchar(100) NOT NULL default '',
  PRIMARY KEY  (contact_id)
) TYPE=MyISAM;

--
-- Table structure for table `site_downtimes`
--

DROP TABLE IF EXISTS site_downtimes;
CREATE TABLE site_downtimes (
  downtime_id int(10) unsigned NOT NULL auto_increment,
  site_id varchar(20) NOT NULL default '',
  description varchar(100) NOT NULL default '',
  start varchar(100) NOT NULL default '',
  endtime varchar(100) NOT NULL default '',
  PRIMARY KEY  (downtime_id)
) TYPE=MyISAM;

--
-- Table structure for table `site_nodes`
--

DROP TABLE IF EXISTS site_nodes;
CREATE TABLE site_nodes (
  node_id int(10) unsigned NOT NULL auto_increment,
  site_id varchar(20) NOT NULL default '',
  type varchar(30) NOT NULL default '',
  machine_name varchar(100) NOT NULL default '',
  comments varchar(100) NOT NULL default '',
  arch varchar(20) NOT NULL default '',
  os varchar(100) NOT NULL default '',
  cpu varchar(100) NOT NULL default '',
  ram varchar(100) NOT NULL default '',
  storage varchar(100) NOT NULL default '',
  middleware varchar(100) NOT NULL default '',
  PRIMARY KEY  (node_id)
) TYPE=MyISAM;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS sites;
CREATE TABLE sites (
  country_id varchar(5) NOT NULL default '',
  site_id varchar(20) NOT NULL default '',
  name varchar(100) default '',
  homepage varchar(100) default NULL,
  giis_url varchar(100) default NULL,
  monitoring varchar(5) default NULL,
  status varchar(20) default NULL,
  type varchar(20) default NULL,
  email varchar(30) default NULL,
  phone varchar(30) default NULL,
  emergency_phone varchar(30) default NULL,
  csirt_email varchar(30) default NULL,
  csirt_phone varchar(30) default NULL,
  hours_available varchar(30) default NULL,
  timezone varchar(100) default NULL,
  OS varchar(30) default NULL,
  middleware varchar(30) default NULL,
  batch_system varchar(30) default NULL,
  VOs_supported varchar(255) default NULL,
  apps_supported text,
  WN_count int(11) default NULL,
  storage_type varchar(30) default '',
  storage_size int(11) default NULL,
  lan_connection float default NULL,
  wan_connection float default NULL,
  comments text,
  admin text,
  timestamp varchar(255) default '',
  latitude float default '0',
  longitude float default '0',
  PRIMARY KEY  (site_id)
) TYPE=MyISAM;

--
-- Table structure for table `timezones`
--

DROP TABLE IF EXISTS timezones;
CREATE TABLE timezones (
  timezone_id varchar(10) NOT NULL default '',
  offset int(11) default NULL,
  PRIMARY KEY  (timezone_id)
) TYPE=MyISAM;

