<?php
/**
 * The constants defined in this file change the behaviour of the 
 * framework and the application. You can change the values of the 
 * constants according to the instructions given in comments, or add 
 * new constants that you use in your application.
 */

/**
 * This is the first page of the application. The framework looks
 * for it at the template folder (specified by TPL). 
 */
define("FIRSTPAGE", "main/main.html");

/**
 * Define the Data Source Name that is used by MDB2 to connect
 * to the database, and the MDB2 options for the default connection.
 */
define('MDB2_DSN', "mysqli://root@127.0.0.1/hgsm?new_link=true");

$MDB2_OPTIONS = array(
                      'debug'       => 2,
                      'portability' => MDB2_PORTABILITY_ALL,
                      );
/**
 * Define the DSN and options of the connection that is used
 * by web_app to connect to the database. If they are not defined
 * then the default ones are used.
 */
/*
define('WEBAPP_MDB2_DSN', "mysqli://root@127.0.0.1/webapp?new_link=true");
$WEBAPP_MDB2_OPTIONS =
  array(
        'debug'       => 2,
        'portability' => MDB2_PORTABILITY_ALL,
        );
*/

/**
 * This constant is the value returned by the framework 
 * for a DB variable that has a NULL value. It can be
 * "", "NULL", NULL, etc.
 */
define("NULL_VALUE", "");

/**
 * This constant sets the format of the error message that is displayed
 * when a {{variable}} is not found. 'var_name' is replaced by
 * the actual variable name. Examples: "'var_name' is not defined",
 * "", "undefined", etc. It cannot contain "{{var_name}}" inside.
 */
define("VAR_NOT_FOUND", "{var_name}");

/**
 * When this constant is true, then the CGI vars are displayed
 * at the URL window of the browser. See also SHOW_EXTERNAL_LINK
 * at const.Debug.php.
 */
define("DISPLAY_CGI_VARS", false);

/**
 * L10N (Translation) Constants
 * The constants LNG and CODESET set the language and codeset of
 * the application. They are used for the localization (translation) of
 * the messages. LNG can be something like 'en_US' or 'en' or UNDEFINED.
 * CODESET can be UNDEFINED, 'iso-latin-1', etc.
 */
define('LNG', 'sq_AL');
define('CODESET', 'iso-latin-1');

/** if true, then use the php-gettext instead of GNU gettext */
define('USE_PHP_GETTEXT', true);

?>