
This folder contains some files and folders that are used
to generate documentation about the code of the application,
using some external tools.

The files and folders that it contains are:
  doxygen.cfg  -- the configuration for doxygen
  phpdoc.sh    -- the configuration for phpDocumentor
  doxydoc/     -- the folder where doxygen outputs the documentation
  phpdoc/      -- the folder where phpDocumentor outputs the documentation

  download/    -- this folder contains the downloadables of the above 
                  documentatations (.tar.gz)
  phpDocumentor/  -- this folder contains the phpDocumentor, which
                  can be downloaded from phpdocu.sourceforge.net

To generate the doxygen documentation, type: "$ doxygen doxygen.cfg".
'doxygen' usually comes with GNU/Linux distributions, but the latest copy
can be downloaded from www.doxygen.org .

To generate the phpDocumentor documentation, type: "$ ./phpdoc.sh".
