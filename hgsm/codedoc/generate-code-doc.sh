#!/bin/bash

### go to this directory
cd $(dirname $0)

### generate HTML and PDF documentation using phpDocumentor
rm -rf hgsm-phpdocu/
./phpdoc_html.sh
./phpdoc_pdf.sh

### generate HTML and PDF documentation using doxygen
rm -rf hgsm-doxygen
/usr/local/bin/doxygen doxygen.cfg
cd hgsm-doxygen/latex/
pdflatex refman
cd ../..

### create phpDocumentor downloadable files
rm -rf download/
mkdir download
tar cfz download/hgsm-phpdocu.tar.gz hgsm-phpdocu/
cp download/documentation.pdf download/hgsm-phpdocu.pdf
gzip download/hgsm-phpdocu.pdf
mv download/documentation.pdf download/hgsm-phpdocu.pdf

### create doxygen downloadable files
tar cfz download/hgsm-doxygen.tar.gz hgsm-doxygen/html/
cp hgsm-doxygen/latex/refman.pdf download/hgsm-doxygen.pdf
gzip download/hgsm-doxygen.pdf
mv hgsm-doxygen/latex/refman.pdf download/hgsm-doxygen.pdf
