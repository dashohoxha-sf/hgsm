<?php
/* This file  is part of HGSM.  HGSM is a  web application for keepinginformation about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you can redistribute it and/or modify it under

the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA*/
/** */

/** return true if the USER is a GOC admin */
function is_goc_admin()
{

  if (IS_GOC_ADMIN=='true')  return true;
  else return false;
}

/** return true if the USER is an admin of the given ROC */
function is_roc_admin($roc_id)
{
  //a GOC admin can also administrate each ROC
  if (is_goc_admin())  return true;

  $query = "SELECT admin FROM ROCs WHERE ROC_id = '$roc_id'";
  $rs = WebApp::sqlQuery($query);
  if ($rs->EOF())  return false;  //$roc_id not valid

  $admin_certs = $rs->Field('admin');
  $arr_admin_certs = explode("\n", trim($admin_certs));
  array_walk($arr_admin_certs, 'str_trim');

  if (in_array(USER, $arr_admin_certs))  return true;
  else return false;
}

/** return true if the USER is an admin of the given country */
function is_country_admin($c_id)
{
  $query = "SELECT ROC_id, admin FROM countries WHERE country_id = '$c_id'";
  $rs = WebApp::sqlQuery($query);
  if ($rs->EOF())  return false;  //country id is not valid

  //a ROC admin can also administrate a country
  $roc_id = $rs->Field('ROC_id');
  if (is_roc_admin($roc_id))  return true;

  $admin_certs = $rs->Field('admin');
  $arr_admin_certs = explode("\n", trim($admin_certs));
  array_walk($arr_admin_certs, 'str_trim');

  if (in_array(USER, $arr_admin_certs))  return true;
  else return false;
}

/** return true if the USER is an admin of the given site */
function is_site_admin($site_id)
{
  $query = "SELECT country_id, admin FROM sites WHERE site_id = '$site_id'";
  $rs = WebApp::sqlQuery($query);
  if ($rs->EOF())  {print "<xmp>$query \n $site_id is not valid</xmp>"; return false;}  //$site_id is not valid

  //a country admin can also administrate a site
  $country_id = $rs->Field('country_id');
  if (is_country_admin($country_id))  return true;

  $admin_certs = $rs->Field('admin');
  $arr_admin_certs = explode("\n", trim($admin_certs));
  array_walk($arr_admin_certs, 'str_trim');

  if (in_array(USER, $arr_admin_certs))  return true;
  else return false;
}

function str_trim(&$str)
{
  $str = trim($str);
}
?>