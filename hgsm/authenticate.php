<?php
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/*
Don't check for the existance and the validity of the certificate,
which means that everybody can view the data.

//check whether the certificate provided by the client browser
//was verified successfully by apache
if ($_SERVER['SSL_CLIENT_VERIFY'] != "SUCCESS")
{
  print T_("The provided certificate could not be verified successfully!");
  exit(0);
}
*/

$arr_goc_admins = 
array(
      '/DC=ORG/DC=SEE-GRID/O=People/O=INIMA/CN=Dashamir Hoxha',
      '/DC=ORG/DC=SEE-GRID/O=People/O=INIMA/CN=Neki Frasheri'
      );

define('USER', $_SERVER['SSL_CLIENT_S_DN']);

$is_goc_admin = in_array(USER, $arr_goc_admins) ? 'true' : 'false';
define('IS_GOC_ADMIN', $is_goc_admin);
?>
