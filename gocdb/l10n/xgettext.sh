#!/bin/bash

### go to this dir
cd $(dirname $0)

### get the translatable strings from '../templates/
app_name=$(./get_app_name.sh)
touch $app_name.po
find ../templates/  -name '*.php'  -print  \
                 -o -name '*.js'  -print   \
                 -o -name '*.html' -print  \
  | xargs xgettext -C --keyword=T_ --join-existing --output=$app_name.po

### get the translatable strings from '../*.php'
ls ../*.php \
  | xargs xgettext -C --keyword=T_ --join-existing --output=$app_name.po
