#!/bin/bash
### create initial translation files for a language

### go to this directory
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 ll_CC"
  echo "where ll_CC is the language code, like en_US or sq_AL"
  exit 1
fi

lng=$1

### create the directory
mkdir -p $lng/LC_MESSAGES/

### create an initial *.po file for the application
app_name=$(./get_app_name.sh)
msginit --input=$app_name.po --locale=$lng --no-translator \
        --output-file=$lng/LC_MESSAGES/$app_name.po
