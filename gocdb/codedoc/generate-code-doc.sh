#!/bin/bash

### go to this directory
cd $(dirname $0)

### generate HTML and PDF documentation using phpDocumentor
rm -rf gocdb-phpdocu/
./phpdoc_html.sh
./phpdoc_pdf.sh

### generate HTML and PDF documentation using doxygen
rm -rf gocdb-doxygen
/usr/local/bin/doxygen doxygen.cfg
cd gocdb-doxygen/latex/
pdflatex refman
cd ../..

### create phpDocumentor downloadable files
rm -rf download/
mkdir download
tar cfz download/gocdb-phpdocu.tar.gz gocdb-phpdocu/
cp download/documentation.pdf download/gocdb-phpdocu.pdf
gzip download/gocdb-phpdocu.pdf
mv download/documentation.pdf download/gocdb-phpdocu.pdf

### create doxygen downloadable files
tar cfz download/gocdb-doxygen.tar.gz gocdb-doxygen/html/
cp gocdb-doxygen/latex/refman.pdf download/gocdb-doxygen.pdf
gzip download/gocdb-doxygen.pdf
mv gocdb-doxygen/latex/refman.pdf download/gocdb-doxygen.pdf
