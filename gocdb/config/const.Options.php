<?php
/**
 * The constants defined in this file change the behaviour of the 
 * framework and the application. You can change the values of the 
 * constants according to the instructions given in comments, or add 
 * new constants that you use in your application.
 */

/**
 * This is the first page of the application. The framework looks
 * for it at the template folder (specified by TPL). 
 */
define("FIRSTPAGE", "main/main.html");

/**
 * if this constant is true, the framework will load the DB component
 * and will open a default connection with the db specified in the
 * file 'config/const.DB.php'
 */
define("USES_DB", true);

/**
 * this constant defines the type of DB that the application uses
 * it can be: "MySQL", "Oracle", "ODBC", "ProgreSQL", etc.
 * (except "MySQL", the others are not implemented yet)
 */
define("DB_TYPE", "MySQL");

/**
 * This constant is the value returned by the framework 
 * for a DB variable that has a NULL value. It can be
 * "", "NULL", NULL, etc.
 */
define("NULL_VALUE", "NULL");

/**
 * This constant sets the format of the error message that is displayed
 * when a {{variable}} is not found. 'var_name' is replaced by
 * the actual variable name. Examples: "'var_name' is not defined",
 * "", "undefined", etc. It cannot contain "{{var_name}}" inside.
 */
define("VAR_NOT_FOUND", "{var_name}");

/**
 * When this constant is true, then the CGI vars are displayed
 * at the URL window of the browser. See also SHOW_EXTERNAL_LINK
 * at const.Debug.php.
 */
define("DISPLAY_CGI_VARS", false);

/**
 * L10N (Translation) Constants
 * The constants LNG and CODESET set the language and codeset of
 * the application. They are used for the localization (translation) of
 * the messages. LNG can be something like 'en_US' or 'en' or UNDEFINED.
 * CODESET can be UNDEFINED, 'iso-latin-1', etc.
 */
define('LNG', 'sq_AL');
define('CODESET', 'iso-latin-1');

/** if true, then use the php-gettext instead of GNU gettext */
define('USE_PHP_GETTEXT', true);

?>