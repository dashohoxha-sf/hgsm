// -*-C-*-
/*
This file  is part of GOCDB.   GOCDB is a web  application for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

GOCDB is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

GOCDB is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with GOCDB; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function set_mode(mode)
{
  SendEvent('country', 'set_mode', 'mode='+mode);
}

function save_country()
{
  var form = document.country_edit;
  var name = form.name.value;
  var email = form.email.value;
  var phone = form.phone.value;
  var comments = form.comments.value;

  var event_args = 'name=' + name + ';'
    + 'email=' + email + ';'
    + 'phone=' + phone + ';'
    + 'comments=' + comments + ';';

  SendEvent('country', 'save', event_args);
}

function change_id()
{
  var form = document.country_edit;
  var country_id = form.country_id.value;

  SendEvent('country', 'change_id', 'country_id='+country_id);
}

function set_admin()
{
  var form = document.country_edit;
  var admin = form.admin.value;

  SendEvent('country', 'set_admin', 'admin='+admin);
}
