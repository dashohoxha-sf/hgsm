<?php
/*
This file  is part of GOCDB.   GOCDB is a web  application for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

GOCDB is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

GOCDB is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with GOCDB; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package main
 */
class main extends WebObject
{
  function init()
    {
      WebApp::addSVar('interface', 'goc');
      WebApp::addSVar('interface_file', 'goc/goc.html');
      WebApp::addSVar('language', 'sq_AL');
    }

  function on_select($event_args)
    {
      $interface = $event_args['interface'];
      switch ($interface)
        {
        default:
        case 'goc':
          $interface_file = 'goc/goc.html';
          break;
        case 'roc':
          $interface_file = 'roc/roc.html';
          $roc_id = $event_args['roc_id'];
          WebApp::setSVar('roc->id', $roc_id);
          WebApp::setSVar('roc->mode', 'view');
          break;
        case 'country':
          $interface_file = 'country/country.html';
          $country_id = $event_args['country_id'];
          WebApp::setSVar('country->id', $country_id);
          WebApp::setSVar('country->mode', 'view');
          break;
        case 'site':
          $interface_file = 'site/site.html';
          $site_id = $event_args['site_id'];
          WebApp::setSVar('site->id', $site_id);
          WebApp::setSVar('site->mode', 'view');
          break;
        }
      WebApp::setSVar('interface', $interface);
      WebApp::setSVar('interface_file', $interface_file);
    }

  function on_language($event_args)
    {
      $lng = $event_args['lng'];
      WebApp::setSVar('language', $lng);
    }

  function onParse()
    {
      $lng = WebApp::getSVar('language');
      global $l10n;
      $l10n->set_lng($lng);
    }

  function onRender()
    {
      //recordset of languages
      $rs_langs = new EditableRS('languages', $query);
      $rs_langs->addRec( array('id'=>'en', 'label'=>'English') );
      $rs_langs->addRec( array('id'=>'sq_AL', 'label'=>'Albanian') );

      //add recordsets to the web page
      global $webPage;
      $webPage->addRecordset($rs_langs);
    }
}
?>