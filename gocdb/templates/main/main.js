// -*-C-*-
/*
This file  is part of GOCDB.   GOCDB is a web  application for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

GOCDB is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

GOCDB is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with GOCDB; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function select(interface_id)
{
  SendEvent('main', 'select', 'interface=' + interface_id);
}

function select_language(list)
{
  var idx = list.selectedIndex;
  var lng = list.options[idx].value;
  SendEvent('main', 'language', 'lng='+lng);
}

function select_goc()
{
  SendEvent('main', 'select', 'interface=goc');
}

function select_roc(roc_id)
{
  var event_args = 'interface=roc;roc_id='+roc_id;
  SendEvent('main', 'select', event_args);
}

function select_site(site_id)
{
  var event_args = 'interface=site;site_id='+site_id;
  SendEvent('main', 'select', event_args);
}

function select_country(country_id)
{
  var event_args = 'interface=country;country_id='+country_id;
  SendEvent('main', 'select', event_args);
}
