<?php
/*
This file  is part of GOCDB.   GOCDB is a web  application for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

GOCDB is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

GOCDB is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with GOCDB; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package roc
 */
class roc extends WebObject
{
  function init()
    {
      $this->addSVar('id', 'SEE-GRID');
      $this->addSVar('mode', 'view');  // view | edit
    }

  function on_set_mode($event_args)
    {
      $mode = $event_args['mode'];

      //(double)check that only a roc admin can go to edit mode
      $roc_id = $this->getSVar('id');
      if ($mode=='edit' and !is_roc_admin($roc_id))  return; 

      if ($mode=='view')
        {
          WebApp::setSVar('country_edit->mode', 'hidden');
        }

      $this->setSVar('mode', $mode);
    }

  function on_save($event_args)
    {
      //(double)check that only a roc admin can save changes
      $roc_id = $this->getSVar('id');
      if (!is_roc_admin($roc_id))  return; 
      
      $event_args['timestamp'] = time();
      WebApp::execDBCmd('save_roc', $event_args);
    }

  function on_change_id($event_args)
    {
      //(double)check that only a GOC admin can do this
      if (!is_goc_admin())  return;

      $new_id = $event_args['ROC_id'];

      //make sure that this id does not already exist
      $rs = WebApp::openRS('get_roc_id', array('roc_id'=>$new_id));
      if (!$rs->EOF())
        {
          WebApp::message(T_("Another ROC with this id already exists!"));
          return;
        }

      //change the roc id in the DB
      $args = array('ROC_id'=>$new_id, 'timestamp'=>time());
      WebApp::execDBCmd('set_roc_id', $args);

      //change the state variable id
      $old_id = $this->getSVar('id');
      $this->setSVar('id', $new_id);

      //change it also in 'countries'
      $args = array('new_roc_id'=>$new_id, 'old_roc_id'=>$old_id);
      WebApp::execDBCmd('update_roc_id', $args);
    }

  function on_set_admin($event_args)
    {
      //(double)check that only a GOC admin can do this
      if (!is_goc_admin())
        {
          WebApp::message(T_("Only a GOC admin can do this!"));
          return;
        }

      $event_args['timestamp'] = time();
      WebApp::execDBCmd('set_admin', $event_args);
    }

  function onRender()
    {
      $rs = WebApp::openRS('roc');
      $vars = $rs->Fields();
      $vars['timestamp'] = date('d/m/Y', $vars['timestamp']);
      WebApp::addVars($vars);

      $roc_id = $this->getSVar('id');
      WebApp::addVar('is_roc_admin', is_roc_admin($roc_id));
    }
}
?>