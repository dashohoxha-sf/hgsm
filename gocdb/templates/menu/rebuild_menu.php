<?php
/*
This file  is part of GOCDB.   GOCDB is a web  application for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

GOCDB is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

GOCDB is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with GOCDB; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package  menu
 */

$dir = dirname(__FILE__);
include_once $dir.'/class.MenuRoot.php';
include_once $dir.'/class.MenuItem.php';
include_once $dir.'/class.DBMenu.php';

//build the menu from the DB data and convert it to JS format
$menu = new DBMenu;
$menu_items_js = $menu->root->to_js();

//write the JS menu to 'menu_items.js'
$fname = $dir.'/menu_items.js';
$fp = fopen($fname, 'w');
fputs($fp, $menu_items_js);
fclose($fp);

//print '<xmp>'; print $menu_items_js; print '</xmp>';  //debug
?>