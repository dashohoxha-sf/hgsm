<?php
/*
This file  is part of GOCDB.   GOCDB is a web  application for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

GOCDB is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

GOCDB is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with GOCDB; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once dirname(__FILE__)."/class.MenuItem.php";

/**
 * @package menu
 */
class MenuRoot extends MenuItem
{
  function MenuRoot($id ="MenuRoot")
    {
      MenuItem::MenuItem($id, 'MenuRoot', 'null');
    }

  /** writes the menu as an XML file */
  function write_xml($fname)
    {
      write_file($fname, $this->to_xml());
    }

  /** writes the menu as a JS file */
  function write_js($fname)
    {
      write_file($fname, $this->to_js());
    }
        
  /** returns the menus as an XML string */
  function to_xml()
    {
      $xml_menu = "<?xml version='1.0'?>
<!DOCTYPE menu SYSTEM 'menu.dtd'>

<menu id='$this->id'>
";
      $xml_menu .= $this->children_to_xml("  ");
      $xml_menu .= "</menu>";
      return $xml_menu;
    }

  /**
   * Returns the JavaScript array with the menu items.
   */
  function to_js()
    {
      $js_arr = "// -*-C-*- \n\n"; 
      $js_arr .= "var MENU_ITEMS =\n[\n"; 

      for ($i=0; $i < $this->nr_children(); $i++)
        {
          $child = $this->children[$i];
          $indent = '  ';
          $js_arr .= $child->to_js_arr($indent);
        }
      $js_arr .= " ];\n";
      return $js_arr;
    }
}
?>