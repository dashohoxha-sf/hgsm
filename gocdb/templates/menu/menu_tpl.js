// -*-C-*- //tell emacs to use the C mode
/*
This file  is part of GOCDB.   GOCDB is a web  application for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

GOCDB is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

GOCDB is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with GOCDB; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

var MENU_POS = [
    {
      // item sizes
      'width': 100,
      'height': 18,
      // menu block offset from the origin:
      //  for root level origin is upper left corner of the page
      //  for other levels origin is upper left corner of parent item
      'block_top': 30,
      'block_left': 0,
      // offsets between items of the same level
      'top': 20,
      'left': 0,
      // time in milliseconds before menu is hidden after cursor has gone out
      // of any items
      'hide_delay': 200,
      'css' : {
        'inner' : 'minner1',
        'outer' : ['moout1', 'moover1', 'modown1']
      }
    },
    {
      'block_top': 20,
      'block_left': 0,
      'css' : {
        'inner' : 'minner',
        'outer' : ['moout', 'moover', 'modown']
      }
    },
    {
      'block_top': 3,
      'block_left': 95
    }
];

