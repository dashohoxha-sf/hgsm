<?php
/*
This file  is part of GOCDB.   GOCDB is a web  application for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

GOCDB is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

GOCDB is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with GOCDB; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package goc
 */
class roc_edit extends formWebObj
{
  var $roc_record = array(
                          'ROC_id' => '',
                          'name'   => '',
                          'email'  => '',
                          'phone'  => '',
                          'admin'  => ''
                          );
  
  function init()
    {
      $this->addSVar('mode', 'hidden');  // add | edit | hidden
      $this->addSVar('ROC_id', UNDEFINED);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          //check that ROC_id does not exist in the table
          $rs = WebApp::openRS('get_roc', $event_args);
          if (!$rs->EOF())
            {
              $this->roc_record = $event_args;
              $ROC_id = $event_args['ROC_id'];
              $msg = T_("ROC ID 'v_ROC_id' is used for another ROC.");
              $msg = str_replace('v_ROC_id', $ROC_id, $msg);
              WebApp::message($msg);
              return;
            }

          //add the new roc
          $event_args['timestamp'] = time();
          WebApp::execDBCmd('add_roc', $event_args);

          //set the new id
          $this->setSVar('ROC_id', $event_args['ROC_id']);

          //rebuild the menu items
          include_once MENU.'/rebuild_menu.php'; 
        }
      else if ($mode=='edit')
        {
          WebApp::execDBCmd('update_roc', $event_args);
        }

      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          WebApp::addVars($this->roc_record);
        }
      else if ($mode=='edit')
        {
          $args = array('ROC_id' => $this->getSVar('ROC_id'));
          $rs = WebApp::openRS('get_roc', $args);
          $vars = $rs->Fields();
          WebApp::addVars($vars);
        }
    }
}
?>