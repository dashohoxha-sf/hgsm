<?php
/*
This file  is part of GOCDB.   GOCDB is a web  application for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

GOCDB is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

GOCDB is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with GOCDB; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package site
 */
class info extends formWebObj
{
  function on_save($event_args)
    {
      //(double)check that only a site admin can save changes
      $site_id = WebApp::getSVar('site->id');
      if (!is_site_admin($site_id))
        {
          WebApp::message(T_("Only a site admin can do this!"));
          return;
        }
      
      $site_info = $event_args;
      $site_info['timestamp'] = time();
      $this->update_record($site_info, 'sites', 'site_id');
    }

  function on_change_id($event_args)
    {
      //(double)check that only a country admin can do this
      $rs = WebApp::openRS('get_country_id');
      $country_id = $rs->Field('country_id');
      if (!is_country_admin($country_id))
        {
          WebApp::message(T_("Only a country admin can do this!"));
          return;
        }

      $new_id = $event_args['site_id'];

      //make sure that this id does not already exist
      $rs = WebApp::openRS('get_site_id', array('site_id'=>$new_id));
      if (!$rs->EOF())
        {
          WebApp::message(T_("Another site with this id already exists!"));
          return;
        }

      //change the site id in the DB
      $args = array('site_id'=>$new_id, 'timestamp'=>time());
      WebApp::execDBCmd('set_site_id', $args);

      //change the state variable site->id
      $old_id = WebApp::getSVar('site->id');
      WebApp::setSVar('site->id', $new_id);

      //change it also in 'site_nodes', 'site_contacts' and 'site_downtimes'
      $args = array('new_site_id'=>$new_id, 'old_site_id'=>$old_id);
      WebApp::execDBCmd('update_nodes_site_id', $args);
      WebApp::execDBCmd('update_contacts_site_id', $args);
      WebApp::execDBCmd('update_downtimes_site_id', $args);
    }

  function on_set_admin($event_args)
    {
      //(double)check that only a country admin can do this
      $rs = WebApp::openRS('get_country_id');
      $country_id = $rs->Field('country_id');
      if (!is_country_admin($country_id))
        {
          WebApp::message(T_("Only a country admin can do this!"));
          return;
        }

      $event_args['timestamp'] = time();
      WebApp::execDBCmd('set_admin', $event_args);
    }

  function onRender()
    {
      $rs = WebApp::openRS('get_site');
      $vars = $rs->Fields();
      $vars['timestamp'] = date('d/m/Y', $vars['timestamp']);
      WebApp::addVars($vars);
    }
}
?>