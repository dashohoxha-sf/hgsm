<?php
/*
This file  is part of GOCDB.   GOCDB is a web  application for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

GOCDB is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

GOCDB is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with GOCDB; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package site
 */
class site extends WebObject
{
  function init()
    {
      $this->addSVar('id', UNDEFINED);
      $this->addSVar('mode', 'view');  // view | edit
    }

  function on_set_mode($event_args)
    {
      $mode = $event_args['mode'];

      //(double)check that only a site admin can go to edit mode
      $site_id = $this->getSVar('id');
      if ($mode=='edit' and !is_site_admin($site_id))  return; 

      if ($mode=='view')
        {
          WebApp::setSVar('contact_edit->mode', 'hidden');
          WebApp::setSVar('node_edit->mode', 'hidden');
          WebApp::setSVar('downtime_edit->mode', 'hidden');
        }

      $this->setSVar('mode', $mode);
    }

  function onRender()
    {
      $rs = WebApp::openRS('get_site_fields');
      $vars = $rs->Fields();
      WebApp::addVars($vars);

      $site_id = $vars['site_id'];
      $country_id = $vars['country_id'];
      WebApp::addVar('is_site_admin', is_site_admin($site_id));
      WebApp::addVar('is_country_admin', is_country_admin($country_id));
    }
}
?>