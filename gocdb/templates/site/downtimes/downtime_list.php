<?php
/*
This file  is part of GOCDB.   GOCDB is a web  application for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

GOCDB is free software; you can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

GOCDB is distributed  in the hope that it will  be useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with GOCDB; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package site
 */
class downtime_list extends WebObject
{
  function init()
    {
      $this->addSVar('downtime_id', UNDEFINED);
    }
    
  function on_add($event_args)
    {
      WebApp::setSVar('downtime_edit->mode', 'add');
      WebApp::setSVar('downtime_edit->downtime_id', UNDEFINED);
    }
    
  function on_edit($event_args)
    {
      WebApp::setSVar('downtime_edit->mode', 'edit');
      WebApp::setSVar('downtime_edit->downtime_id', $event_args['downtime_id']);
    }

  function on_del($event_args)
    {
      WebApp::execDBCmd('del_downtime', $event_args);
    }    

  function onRender()
    {
    }
}
?>