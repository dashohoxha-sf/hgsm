#! /usr/bin/perl
use Spreadsheet::ParseExcel;

# variable to (row, col) mapping
@vars = (
    ["country", 0, 1],
    ["site", 1, 3],
    ["domain", 1, 6],
    ["date", 1, 9],
    ["aname", 3, 3],
    ["amail", 4, 3],
    ["aphone", 5, 3],
    ["sname", 6, 3],
    ["smail", 7, 3],
    ["sphone", 8, 3],
    ["nnodes", 10, 3],
    ["ctype", 11, 3],
    ["stype", 12, 3],
    ["ssize", 13, 3],
    ["lan", 14, 3],
    ["geant", 15, 3],
    ["wan", 16, 3],
    ["rh", 17, 3],
    ["sl", 18, 3],
    ["os", 19, 3],
    ["mw", 20, 3],
    ["pbs", 21, 3],
    ["lsf", 22, 3],
    ["condor", 23, 3],
    ["batch", 24, 3],
    ["inst", 25, 3],
    ["cert_loc", 26, 3],
    ["cert_egee", 27, 3],
    ["atlas", 10, 8],
    ["alice", 11, 8],
    ["lhcb", 12, 8],
    ["cms", 13, 8],
    ["hep_o", 14, 7],
    ["gate", 15, 8],
    ["cdss", 16, 8],
    ["gpsa", 17, 8],
    ["simri3d", 18, 8],
    ["gptm3d", 19, 8],
    ["xmiffmlref", 20, 8],
    ["gramm", 21, 8],
    ["bio_o", 22, 7],
    ["esr", 24, 8],
    ["astro", 25, 8],
    ["chem", 26, 8],
    ["se4see", 27, 8],
    ["vive", 28, 8],
    ["gen_o", 29, 7],
);

%user2cert = (
"Alexandru STANCIU" => "/DC=ORG/DC=SEE-GRID/O=People/O=ICI/CN=Alexandru Stanciu",
"Antun Bala~" => "/DC=ORG/DC=SEE-GRID/O=People/O=UOB/CN=Antun Balaz",
"Boro Jakimovski" => "/DC=ORG/DC=SEE-GRID/O=People/O=UKIM/CN=Boro Jakimovski",
"Emanouil Atanassov" => "/O=GRID-FR/C=BG/O=BAS/OU=IPP/CN=Emanouil Atanassov",
"Evren Karaca" => "/DC=ORG/DC=SEE-GRID/O=People/O=TUBITAK/CN=Evren Karaca",
"Ivan Lirkov" => "/DC=ORG/DC=SEE-GRID/O=People/O=IPP-BAS/CN=Ivan Lirkov",
"Ivica Bariai^G" => "/DC=ORG/DC=SEE-GRID/O=People/O=UOB/CN=Ivica Barisic",
"J�zsef Patvarczki" => "/C=HU/O=KFKI RMKI CA/OU=SZTAKI/CN=Patvarczki Jozsef",
"Kalin Lilovski" => "/DC=ORG/DC=SEE-GRID/O=People/O=IM-BAS/CN=Kalin Lilovski",
"Luchesar Iliev" => "/DC=ORG/DC=SEE-GRID/O=People/O=IPP-BAS/CN=Luchesar Iliev",
"Neki Frasheri" => "/DC=ORG/DC=SEE-GRID/O=People/O=INIMA/CN=Neki Frasheri",
"Onur Temizsoylu" => "/DC=ORG/DC=SEE-GRID/O=People/O=TUBITAK/CN=Onur Temizsoylu",
"Titi Preda " => "/O=GRID-FR/C=RO/O=NIPNE/OU=DIC/CN=Titi Preda",
"Valentin Vidic" => "/DC=ORG/DC=SEE-GRID/O=People/O=RBI/CN=Valentin Vidic",
"Vladimir Dimitrov" => "/DC=ORG/DC=SEE-GRID/O=People/O=IPP-BAS/CN=Vladimir Dimitrov",
);

%site2name = (
"INIMA" => "Institute of Informatics and Applied Mathematics",
"BG01-IPP" => "Institute for Parallel Processing, Bulgarian Academy of Sciences",
"BG02-IM" => "Institute of Mechanics, Bulgarian Academy of Sciences",
"BG03-IPP-N" => "Institute for Parallel Processing",
"BG04-ACAD" => "IPP (Institute for Parallel Processing), ISTF (Information Society Technologies Foundation)",
"HR-01-RBI" => "Rudjer Boskovic Institute",
"test-Grnet" => "Network Management & Optimal Design Laboratory",
"SZTAKI" => "Computer and Automation Research Institute, Hungarian Academy of Sciences",
"MK03" => "Macedonian Academic and Research Network (MARNET), University of Sts Cyril and Methodius, Skopje",
"RO-01-ICI" => "National Institute for Research and Development in Informatics, Bucharest",
"RO-03-UPB" => "UNK",
"AEGIS01-PHY-SCL" => "Scientific Computing Laboratory, Institute of Physics, Belgrade",
"AEGIS02-RCUB" => "Computing Centre, University of Belgrade",
"TR-01-ULAKBIM" => "Turkish Academic Network and Information Center",
"TR-02-BILKENT" => "Bilkent University, Computer Engineering Department, Ankara",
"TR-03-METU" => "Middle East Technical University, Computer Center, Ankara",
);

$xls = Spreadsheet::ParseExcel::Workbook->Parse($ARGV[0]);

# create ROCs
$roc_id = 'SEEGRID';
print "INSERT INTO ROCs VALUES('$roc_id', 'South-East Europe GRID', ",
    "'/C=GR/O=HellasGrid/OU=grnet.gr/CN=Ognjen Prnjat', ",
    "'helpdesk\@seegrid.org', '', '', '');\n";

# create VOs
foreach $vo (atlas, alice, cms, lhcb, sixt, na48, dteam, see, seegrid,
    biomed, esr) {
    print "INSERT INTO VOs VALUES ('$vo', '$vo');\n";
}

foreach $tbl (@{$xls->{Worksheet}}) {
    # not interested in the first/last one
    next if $tbl->{Name} eq "SEE_GRID" or
            $tbl->{Name} eq "CountryName_ClusterName";

    # get site data into vars
    foreach (@vars) {
        ($var, $row, $col) = @$_;

        $val = &get_cell($row, $col);
	$val =~ s/@/\\\@/g; # or emails get destroyed in eval

        eval "\$$var=\"$val\"";

        # print "$var: $val\n";
    }

    # create country
    if (! $c{$country}) {
        $c{$country} = 1;
        $country_id = uc substr($country, 0, 2);
        print "INSERT INTO countries VALUES ('$country_id', '$roc_id', ",
            "'$country', '', '', '', '', '');\n";
    }

    # merge os fields
    $os_all  = "";
    $os_all .= ", RH $rh"   if $rh;
    $os_all .= ", SL $sl" if $sl;
    $os_all .= ", $os"    if $os;
    $os_all =~ s/^, //;

    # merge batchsys fields
    $batch_all  = "";
    $batch_all .= ", PBS $pbs"         if $pbs;
    $batch_all .= ", LSF $lsf"       if $lsf;
    $batch_all .= ", Condor $condor" if $condor;
    $batch_all .= ", $batch"         if $batch;
    $batch_all =~ s/^, //;
    $batch_all =~ s/ x, /, /gi;
    $batch_all =~ s/ x$//gi;

    # merge apps
    $apps  = "";
    foreach (@vars) {
        ($var, $row, $col) = @$_;
        if ($col == 8) {
            $apps .= ", $var" if eval "\$$var";
        }
    }

    # other apps
    $hep_o =~ s/other \((.*)\)/$1/g;
    $bio_o =~ s/other \((.*)\)/$1/g;
    $gen_o =~ s/other \((.*)\)/$1/g;

    $apps .= ", $hep_o"     if $hep_o ne "please insert";
    $apps .= ", $bio_o"     if $bio_o ne "please insert";
    $apps .= ", $gen_o"     if $gen_o ne "please insert";

    $apps =~ s/^, //;
    $apps =~ s/\(x\)//gi;
    $apps =~ s/\(yes\)//gi;

    # merge vos
    @vos = (dteam);
    push @vos, "atlas"   if $atlas;
    push @vos, "alice"   if $alice;
    push @vos, "lhcb"    if $lhcb;
    push @vos, "cms"     if $cms;
    push @vos, "esr"     if $esr; 
    push @vos, "seegrid" if $se4see || $vive;
    push @vos, "biomed"  if $gate || $cdss || $gpsa || $simri3d || $gptm3d ||
        $xmiffmlref || $gramm || $bio_o || $overall;

    $vos = join(", ", @vos);

    # merge admins
    %adm = ();
    foreach (split /\//, "$aname/$sname") {
        $adm{$user2cert{$_}} = $_ if $user2cert{$_};
    }
    $adm = join(",", keys %adm);

    # merge wan info
    $geant *= 1000 if $geant =~ s/gb.*//i;
    $wan *= 1000 if $geant =~ s/gb.*//i;
    $geant =~ s/mb.*//i;
    $wan =~ s/mb.*//i;
    $wan_max = $geant > $wan ? $geant : $wan;
    $wan_max *= 1; # undef -> 0

    # lan 
    $lan *= 1000 if $lan =~ s/gb.*//i;
    $lan =~ s/mb.*//i;
    $lan =~ s/\/.*//;
    $lan *= 1;

    # tb -> gb
    $ssize = int($ssize * 1000);

    # get long site name (if known)
    $site_name = $site;
    $site_name = $site2name{$site} if $site2name{$site};

    # create site
    print "INSERT INTO sites VALUES('$country_id', '$site', '$site_name', ",
        "'http://$domain/', '', 1, 1, 1, '$amail', '$aphone', ",
        "'$sphone', '$smail', '$sphone', '08:00-17:00', 1, '$os_all', ",
        "'$mw', '$batch_all', '$vos', '$apps', 0, '$stype', ", $ssize + 0, 
        ", ", $lan + 0, ", ", $wan_max, ", '', '$adm', '');\n";
    
    print "INSERT INTO site_contacts VALUES(0, '$site', '$aname', ",
        "'$amail', '$aphone', 'System administrator of $site', ",
        "'08:00-17:00');\n" if $aname;
    print "INSERT INTO site_contacts VALUES(0, '$site', '$sname', ",
        "'$smail', '$sphone', 'Security contact for $site', ",
        "'08:00-17:00');\n" if $sname;

    # count wns
    $wns = 0;

    # get node data
    for ($i = 35; $n_name = &get_cell($i, 1); $i++) {
        $n_mw = &get_cell($i, 2);
        $n_svc = &get_cell($i, 4);
        $n_arch = &get_cell($i, 5);
        $n_cpu = &get_cell($i, 6);
        $n_mem = &get_cell($i, 7);
        $n_disk = &get_cell($i, 8);
        $n_os = &get_cell($i, 9);

        # append domain
        $n_name .= ".$domain" if ($n_name !~ /\./);

        # types are separated by +
        $n_svc =~ s/\//+/g;
        $n_svc =~ s/,/+/g;
        $n_svc =~ s/ *//g;

        print "INSERT INTO site_nodes VALUES(", 0,
              ", '$site', '$n_svc', '$n_name', '', '$n_arch', ",
              "'$n_os', '$n_cpu', '$n_mem', '$n_disk', '$n_mw');\n";
        
        # site bdii runs on CE
        if ($n_svc =~ /^CE|\+CE/) {
            print "UPDATE sites SET giis_url=",
                "'ldap://$n_name:2170/mds-vo-name=$site,o=grid' ",
                "WHERE site_id='$site';\n";
        }

        # count wns
        $wns++ if ($n_svc =~ /^WN|\+WN/);
    }

    print "UPDATE sites SET WN_count='$wns' WHERE site_ID='$site';\n";

    print "\n";
}

# get cell value (or "" of empty)
sub get_cell {
    my ($row, $col) = @_;
    my $cell = $tbl->{Cells}[$row][$col];
    my $val = $cell ? $cell->{Val} : "";

    $val =~ s/ //g;
    return $val;
}
