-- MySQL dump 8.23
--
-- Host: 127.0.0.1    Database: gocdb
---------------------------------------------------------
-- Server version	3.23.58

--
-- Current Database: gocdb
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ gocdb;

USE gocdb;

--
-- Table structure for table `ROCs`
--

DROP TABLE IF EXISTS ROCs;
CREATE TABLE ROCs (
  ROC_id varchar(10) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  admin text NOT NULL,
  email varchar(255) NOT NULL default '',
  phone varchar(255) NOT NULL default '',
  comments text,
  timestamp varchar(255) NOT NULL default '',
  PRIMARY KEY  (ROC_id)
) TYPE=MyISAM;

--
-- Dumping data for table `ROCs`
--


INSERT INTO ROCs (ROC_id, name, admin, email, phone, comments, timestamp) VALUES ('SEEGRID','South-East Europe GRID','/C=GR/O=HellasGrid/OU=grnet.gr/CN=Ognjen Prnjat','helpdesk@seegrid.org','','','');

--
-- Table structure for table `VOs`
--

DROP TABLE IF EXISTS VOs;
CREATE TABLE VOs (
  VO_id varchar(10) NOT NULL default '',
  name varchar(30) default NULL,
  PRIMARY KEY  (VO_id)
) TYPE=MyISAM;

--
-- Dumping data for table `VOs`
--


INSERT INTO VOs (VO_id, name) VALUES ('atlas','atlas');
INSERT INTO VOs (VO_id, name) VALUES ('alice','alice');
INSERT INTO VOs (VO_id, name) VALUES ('cms','cms');
INSERT INTO VOs (VO_id, name) VALUES ('lhcb','lhcb');
INSERT INTO VOs (VO_id, name) VALUES ('sixt','sixt');
INSERT INTO VOs (VO_id, name) VALUES ('na48','na48');
INSERT INTO VOs (VO_id, name) VALUES ('dteam','dteam');
INSERT INTO VOs (VO_id, name) VALUES ('see','see');
INSERT INTO VOs (VO_id, name) VALUES ('seegrid','seegrid');
INSERT INTO VOs (VO_id, name) VALUES ('biomed','biomed');
INSERT INTO VOs (VO_id, name) VALUES ('esr','esr');

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS countries;
CREATE TABLE countries (
  country_id varchar(5) NOT NULL default '',
  ROC_id varchar(10) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  admin text NOT NULL,
  email varchar(255) NOT NULL default '',
  phone varchar(255) NOT NULL default '',
  comments text,
  timestamp varchar(255) NOT NULL default '',
  PRIMARY KEY  (country_id)
) TYPE=MyISAM;

--
-- Dumping data for table `countries`
--


INSERT INTO countries (country_id, ROC_id, name, admin, email, phone, comments, timestamp) VALUES ('AL','SEEGRID','Albania','','','','','');
INSERT INTO countries (country_id, ROC_id, name, admin, email, phone, comments, timestamp) VALUES ('BU','SEEGRID','Bulgaria','','','','','');
INSERT INTO countries (country_id, ROC_id, name, admin, email, phone, comments, timestamp) VALUES ('SE','SEEGRID','Serbia & Montenegro','','','','','');
INSERT INTO countries (country_id, ROC_id, name, admin, email, phone, comments, timestamp) VALUES ('GR','SEEGRID','Greece','','','','','');
INSERT INTO countries (country_id, ROC_id, name, admin, email, phone, comments, timestamp) VALUES ('CR','SEEGRID','Croatia','','','','','');
INSERT INTO countries (country_id, ROC_id, name, admin, email, phone, comments, timestamp) VALUES ('HU','SEEGRID','Hungary','','','','','');
INSERT INTO countries (country_id, ROC_id, name, admin, email, phone, comments, timestamp) VALUES ('MA','SEEGRID','Macedonia','','','','','');
INSERT INTO countries (country_id, ROC_id, name, admin, email, phone, comments, timestamp) VALUES ('RO','SEEGRID','Romania','','','','','');
INSERT INTO countries (country_id, ROC_id, name, admin, email, phone, comments, timestamp) VALUES ('TU','SEEGRID','Turkey','','','','','');

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS session;
CREATE TABLE session (
  id varchar(255) NOT NULL default '',
  vars text NOT NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Dumping data for table `session`
--


INSERT INTO session (id, vars) VALUES ('IP: 192.168.252.2; DATE: 2006-01-18 15:06:06','a:0:{}');

--
-- Table structure for table `site_contacts`
--

DROP TABLE IF EXISTS site_contacts;
CREATE TABLE site_contacts (
  contact_id int(10) unsigned NOT NULL auto_increment,
  site_id varchar(20) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  email varchar(30) NOT NULL default '',
  phone varchar(30) NOT NULL default '',
  role varchar(100) NOT NULL default '',
  hours varchar(100) NOT NULL default '',
  PRIMARY KEY  (contact_id)
) TYPE=MyISAM;

--
-- Dumping data for table `site_contacts`
--


INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (1,'INIMA','Neki Frasheri','nfra@inima.al','+355 4 22 88 52','System administrator of INIMA','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (2,'INIMA','Marenglen Bukli','mbuk@inima.al','+355 4 22 62 34','Security contact for INIMA','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (3,'BG02-IM','Kalin Lilovski','egee-site-admins@imbm.bas.bg','+359 2 979 2860','System administrator of BG02-IM','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (4,'BG02-IM','Stefan Karatanchev','egee-security@imbm.bas.bg','+359 2 979 2860','Security contact for BG02-IM','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (5,'BG03-IPP-N','Ivan Lirkov','seegrid-admin@parallel.bas.bg','+359 2 979 6629','System administrator of BG03-IPP-N','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (6,'BG03-IPP-N','Emanouil Atanassov','seegrid-security@parallel.bas.','+359 2 979 6793','Security contact for BG03-IPP-N','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (7,'BG04-ACAD','Vladimir Dimitrov','vgd@acad.bg','+359 2 979 6615','System administrator of BG04-ACAD','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (8,'BG04-ACAD','Luchesar Iliev','grid-csirt@acad.bg','+359 2 979 6614','Security contact for BG04-ACAD','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (9,'BG01-IPP','Emanouil Atanassov','egee-admin@parallel.bas.bg','+359 2 979 6793','System administrator of BG01-IPP','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (10,'BG01-IPP','Kiril Tchorbadjiyski','egee-security@parallel.bas.bg','35929796609','Security contact for BG01-IPP','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (11,'AEGIS02-RCUB','Ivica Barisic','ivica@rcub.bg.ac.yu','+381 11 3031257','System administrator of AEGIS02-RCUB','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (12,'AEGIS02-RCUB','Ivica Barisic','ivica@rcub.bg.ac.yu','+381 11 3031257','Security contact for AEGIS02-RCUB','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (13,'AEGIS01-PHY-SCL','Antun Bala~','antun@phy.bg.ac.yu','+381 11 3160260 Ext 152','System administrator of AEGIS01-PHY-SCL','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (14,'AEGIS01-PHY-SCL','Antun Bala~','antun@phy.bg.ac.yu','+381 11 3160260 Ext 152','Security contact for AEGIS01-PHY-SCL','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (15,'test-Grnet','Athanasios Moralis','amoral@netmode.ntua.gr','302107721449','System administrator of test-Grnet','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (16,'test-Grnet','Athanasios Moralis','amoral@netmode.ntua.gr','302107721449','Security contact for test-Grnet','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (17,'HR-01-RBI','Valentin Vidic','lcg-admin@irb.hr','+387(1)4651111 ext. 1671','System administrator of HR-01-RBI','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (18,'HR-01-RBI','Nikola Pavkovic','lcg-admin@irb.hr','+387(1)4651111 ext. 1671','Security contact for HR-01-RBI','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (19,'SZTAKI','J�zsef Patvarczki','patvarc@sztaki.hu','+361 2796066','System administrator of SZTAKI','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (20,'MK01','Aleksandar Dimeski','cano@marnet.net.mk','+38923129068','System administrator of MK01','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (21,'MK01','Goran Muratovski','gone@marnet.net.mk','+38923109324','Security contact for MK01','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (22,'MK02','Boro Jakimovski','boroj@ii.edu.mk','+389 70 328 718','System administrator of MK02','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (23,'MK02','Goran Velinov','goran@','+389 70 752 989','Security contact for MK02','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (24,'MK03','Dragan Andonov','dan@etf.ukim.edu.mk','+389 70 323 765','System administrator of MK03','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (25,'RO-01-ICI','Alexandru STANCIU/Alexandru TUDOSE','alex@ici.ro/atudose@rnc.ro','+40 21 224 12 56','System administrator of RO-01-ICI','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (26,'RO-01-ICI','Alexandru STANCIU/Alexandru TUDOSE','alex@ici.ro/atudose@rnc.ro','+40 21 224 52 62','Security contact for RO-01-ICI','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (27,'RoGrid-ICI-GILDA','Vladimir FLORIAN','vladimir@ici.ro','+40 21 224 07 36 ext. 159','System administrator of RoGrid-ICI-GILDA','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (28,'RoGrid-ICI-GILDA','Vladimir FLORIAN','vladimir@ici.ro','+40 21 224 52 62','Security contact for RoGrid-ICI-GILDA','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (29,'RoGrid-INCAS','Catalin Nae','cnae@incas.ro','+40745780140','System administrator of RoGrid-INCAS','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (30,'RoGrid-INCAS','Catalin Nae','cnae@incas.ro','+40745780140','Security contact for RoGrid-INCAS','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (31,'RoGrid-NIPNE-01','Titi Preda/Mihai Ciubancan','Titi.Preda@nipne.ro/Mihai.Ciub','+40 21 4042361','System administrator of RoGrid-NIPNE-01','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (32,'RoGrid-NIPNE-01','Titi Preda/Mihai Ciubancan','Titi.Preda@nipne.ro/Mihai.Ciub','+40 21 4042361','Security contact for RoGrid-NIPNE-01','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (33,'TR-01-ULAKBIM','Onur Temizsoylu','onurt@ulakbim.gov.tr','90 312 298 93 63','System administrator of TR-01-ULAKBIM','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (34,'TR-01-ULAKBIM','Onur Temizsoylu','onurt@ulakbim.gov.tr','90 312 298 93 63','Security contact for TR-01-ULAKBIM','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (35,'BilkentGrid','Evren Karaca','ekaraca@cs.bilkent.edu.tr','+90 312 2901401','System administrator of BilkentGrid','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (36,'BilkentGrid','Tayfun K���ky1lmaz','ktayfun@cs.bilkent.edu.tr','+90 312 2901401','Security contact for BilkentGrid','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (37,'TR-03-METU','Onur Temizsoylu','onurt@ulakbim.gov.tr','90 312 298 93 63','System administrator of TR-03-METU','08:00-17:00');
INSERT INTO site_contacts (contact_id, site_id, name, email, phone, role, hours) VALUES (38,'TR-03-METU','Onur Temizsoylu','onurt@ulakbim.gov.tr','90 312 298 93 63','Security contact for TR-03-METU','08:00-17:00');

--
-- Table structure for table `site_downtimes`
--

DROP TABLE IF EXISTS site_downtimes;
CREATE TABLE site_downtimes (
  downtime_id int(10) unsigned NOT NULL auto_increment,
  site_id varchar(20) NOT NULL default '',
  description varchar(100) NOT NULL default '',
  start varchar(100) NOT NULL default '',
  endtime varchar(100) NOT NULL default '',
  PRIMARY KEY  (downtime_id)
) TYPE=MyISAM;

--
-- Dumping data for table `site_downtimes`
--



--
-- Table structure for table `site_nodes`
--

DROP TABLE IF EXISTS site_nodes;
CREATE TABLE site_nodes (
  node_id int(10) unsigned NOT NULL auto_increment,
  site_id varchar(20) NOT NULL default '',
  type varchar(30) NOT NULL default '',
  machine_name varchar(100) NOT NULL default '',
  comments varchar(100) NOT NULL default '',
  arch varchar(20) NOT NULL default '',
  os varchar(100) NOT NULL default '',
  cpu varchar(100) NOT NULL default '',
  ram varchar(100) NOT NULL default '',
  storage varchar(100) NOT NULL default '',
  middleware varchar(100) NOT NULL default '',
  PRIMARY KEY  (node_id)
) TYPE=MyISAM;

--
-- Dumping data for table `site_nodes`
--


INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (1,'INIMA','CE','prof.grid.inima.al','','i386','SL 3.0.4','Intel P3 .08GHz','0.128','0.005','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (2,'INIMA','WN+UI','v101.grid.inima.al','','i386','SL 3.0.4','Intel P3 .08GHz','0.128','0.005','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (3,'INIMA','WN+UI','v102.grid.inima.al','','i386','SL 3.0.4','Intel P3 .08GHz','0.128','0.005','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (4,'INIMA','WN+UI','v119.grid.inima.al','','i386','SL 3.0.4','Intel P3 .08GHz','0.128','0.005','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (5,'INIMA','WN+UI','v120.grid.inima.al','','i386','SL 3.0.4','Intel P3 .08GHz','0.128','0.005','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (6,'BG02-IM','ComputingElementSet','ce001.imbm.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 -HT 2,8GHz',' 0.256','  0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (7,'BG02-IM','StorigeElementSet','se001.imbm.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 -HT 2,8GHz',' 0.256','  0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (8,'BG02-IM','LCFGserver.DNS','quant.imbm.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 -HT 2,8GHz',' 0.256','  0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (9,'BG02-IM','PBSClient','wn001.imbm.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 -HT 2,8GHz',' 0.256','  0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (10,'BG02-IM','PBSClient','wn002.imbm.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 -HT 2,8GHz',' 0.256','  0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (11,'BG02-IM','PBSClient','wn003.imbm.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 -HT 2,8GHz',' 0.256','  0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (12,'BG02-IM','PBSClient','wn004.imbm.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 -HT 2,8GHz',' 0.256',' 0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (13,'BG02-IM','PBSClient','wn005.imbm.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 -HT 2,8GHz',' 0.256','  0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (14,'BG02-IM','UserInterfaceSet','ui001.imbm.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 -HT 2,8GHz',' 0.256','  0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (15,'BG03-IPP-N','PBSClient','wn01.seegrid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4-HT 2,8GHz','1',' 0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (16,'BG03-IPP-N','PBSClient','wn02.seegrid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4-HT 2,8GHz','1','0.04','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (17,'BG03-IPP-N','PBSClient','wn03.seegrid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4-HT 2,8GHz','1','0.02','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (18,'BG03-IPP-N','CE+UI','ce2.seegrid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4  1.5GHz',' 0,256','0.03','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (19,'BG03-IPP-N','SE','se2.seegrid.bas.bg','','i386','Scientific Linux 3.0.3, 2.4.20-27','AMD Seleron  2,0GHz',' 0,256',' 0,020','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (20,'BG04-ACAD','WN','wn01.grid.acad.bg','','i386','Scientific Linux v3.03','2,8GHz Intel P4-HT','1','0.04','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (21,'BG04-ACAD','WN','wn02.grid.acad.bg','','i386','Scientific Linux v3.03','2,8GHz Intel P4-HT','1','0.04','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (22,'BG04-ACAD','WN','wn03.grid.acad.bg','','i386','Scientific Linux v3.03','2,8GHz Intel P4-HT','1','0.04','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (23,'BG04-ACAD','WN','wn04.grid.acad.bg','','i386','Scientific Linux v3.03','2,8GHz Intel P4-HT','1','0.04','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (24,'BG04-ACAD','WN','wn05.grid.acad.bg','','i386','Scientific Linux v3.03','2,8GHz Intel P4-HT','1','0.04','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (25,'BG04-ACAD','WN','wn06.grid.acad.bg','','i386','Scientific Linux v3.03','2,8GHz Intel P4-HT','1','0.04','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (26,'BG04-ACAD','CE+UI','ce01.grid.acad.bg','','i386','Scientific Linux v3.03','2,0GHz Intel P4-Xeon',' 0,768','0.072','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (27,'BG04-ACAD','SE','se01.grid.acad.bg','','i386','Scientific Linux v3.03','2,8GHz Intel P4-HT',' 0,512','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (28,'BG01-IPP','ComputingElementSet','ce001.grid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 2,4GHz','0.5',' 0,080','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (29,'BG01-IPP','StorigeElementSet','se001.grid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 2,4GHz','0.5',' 0,080','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (30,'BG01-IPP','UserInterfaceSet','ca.grid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4 2,0GHz','0.256',' 0,080','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (31,'BG01-IPP','PBSClient','wn001.grid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27 SMP','Intel P4-HT 2,8GHz','1','  0,120','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (32,'BG01-IPP','PBSClient','wn002.grid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27 SMP','Intel P4-HT 2,8GHz','1','  0,120','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (33,'BG01-IPP','PBSClient','wn003.grid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27 SMP','Intel P4 2,4GHz','1','  0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (34,'BG01-IPP','PBSClient','wn004.grid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27',' Intel P4  2,4GHz','1','  0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (35,'BG01-IPP','PBSClient','wn007.grid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27','Intel P4-HT 2,8GHz','1','  0,040','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (36,'BG01-IPP','PBSClient','wn008.grid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27 SMP','Intel P4-HT 2,8GHz','1','0.12','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (37,'BG01-IPP','PBSClient','wn010.grid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27 SMP','Intel P4-HT 2,8GHz','1','  0,120','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (38,'BG01-IPP','PBSClient','wn011.grid.bas.bg','','i686','Scientific Linux 3.0.3, 2.4.20-27 SMP','Intel P4-HT 2,8GHz','1','  0,120','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (39,'AEGIS02-RCUB','CE+SE','grid01.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.3','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (40,'AEGIS02-RCUB','RB+BDII+UI','grid02.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (41,'AEGIS02-RCUB','WN','grid03.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (42,'AEGIS02-RCUB','WN','grid04.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (43,'AEGIS02-RCUB','WN','grid05.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (44,'AEGIS02-RCUB','WN','grid06.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (45,'AEGIS02-RCUB','WN','grid07.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (46,'AEGIS02-RCUB','WN','grid08.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (47,'AEGIS02-RCUB','WN','grid09.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (48,'AEGIS02-RCUB','WN','grid12.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (49,'AEGIS02-RCUB','WN','grid13.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (50,'AEGIS02-RCUB','WN','grid14.rcub.bg.ac.yu','','i686','SL 3.0.4','AMD Sempron 2800+ 2.0GHz','1','0.08','LCG 2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (51,'AEGIS01-PHY-SCL','UI+CE','ce.phy.bg.ac.yu','','i686','SL 3.0.4','AMD Athlon 1.33 GHz','0.512','0.12','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (52,'AEGIS01-PHY-SCL','SE+GridICE','se.phy.bg.ac.yu','','i686','SL 3.0.4','AMD Athlon 1.33 GHz','0.512','0.08','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (53,'AEGIS01-PHY-SCL','MON+PX+BDII','grid.phy.bg.ac.yu','','i686','SL 3.0.4','AMD Athlon 1.33 GHz','0.512','0.04','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (54,'AEGIS01-PHY-SCL','RB','rb.phy.bg.ac.yu','','i686','SL 3.0.4','AMD Athlon 1.33 GHz','0.768','0.04','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (55,'AEGIS01-PHY-SCL','WN','wn01.phy.bg.ac.yu','','i686','SL 3.0.4','2 x Xeon 2.8 GHz','1','0.04','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (56,'AEGIS01-PHY-SCL','WN','wn02.phy.bg.ac.yu','','i686','SL 3.0.4','2 x Xeon 2.8 GHz','1','0.04','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (57,'AEGIS01-PHY-SCL','WN','wn03.phy.bg.ac.yu','','i686','SL 3.0.4','2 x Xeon 2.8 GHz','1','0.04','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (58,'AEGIS01-PHY-SCL','WN','wn04.phy.bg.ac.yu','','i686','SL 3.0.4','2 x Xeon 2.8 GHz','1','0.04','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (59,'AEGIS01-PHY-SCL','WN','wn05.phy.bg.ac.yu','','i686','SL 3.0.4','2 x Xeon 2.8 GHz','1','0.04','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (60,'AEGIS01-PHY-SCL','WN','wn06.phy.bg.ac.yu','','i686','SL 3.0.4','2 x Xeon 2.8 GHz','1','0.04','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (61,'AEGIS01-PHY-SCL','WN','wn07.phy.bg.ac.yu','','i686','SL 3.0.4','2 x Xeon 2.8 GHz','1','0.04','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (62,'AEGIS01-PHY-SCL','WN','wn08.phy.bg.ac.yu','','i686','SL 3.0.4','2 x Xeon 2.8 GHz','1','0.04','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (63,'test-Grnet','CE+SE','grid1.netmode.ece.ntua.gr','','i386','Redhat 7.3','Intel Celeron 466','0.256','0.008','LCG 2.2.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (64,'test-Grnet','RB+BDII','grid2.netmode.ece.ntua.gr','','i386','Redhat 7.3','Intel P2 350','0.256','0.01','LCG 2.2.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (65,'test-Grnet','WN','grid3.netmode.ece.ntua.gr','','i386','Redhat 7.3','Intel Pentium Pro 200','0.128','0.008','LCG 2.2.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (66,'test-Grnet','UI','grid4.netmode.ece.ntua.gr','','i386','Redhat 7.3','Intel P3 833','0.256','0.02','LCG 2.2.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (67,'test-Grnet','','grid5.netmode.ece.ntua.gr','','i386','Scientific Linux 3.03','Intel Dual Xeon 2.8 GH','1.024','0.08','');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (68,'HR-01-RBI','CE','grid1.irb.hr','','i386','Scientific Linux 3.0.3/Debian Sarge','2x2.8GHz Intel Xeon with HT','2','0.03','2_4_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (69,'HR-01-RBI','WN','node1.irb.hr','','i386','Scientific Linux 3.0.3/Debian Sarge','2x2.8GHz Intel Xeon with HT','2','0.03','2_4_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (70,'HR-01-RBI','WN','node2.irb.hr','','i386','Scientific Linux 3.0.3/Debian Sarge','2x2.8GHz Intel Xeon with HT','2','0.03','2_4_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (71,'HR-01-RBI','WN','node3.irb.hr','','i386','Scientific Linux 3.0.3/Debian Sarge','2x2.8GHz Intel Xeon with HT','2','0.03','2_4_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (72,'HR-01-RBI','WN','node4.irb.hr','','i386','Scientific Linux 3.0.3/Debian Sarge','2x2.8GHz Intel Xeon with HT','2','0.03','2_4_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (73,'HR-01-RBI','WN','node5.irb.hr','','i386','Scientific Linux 3.0.3/Debian Sarge','2x2.8GHz Intel Xeon with HT','2','0.03','2_4_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (74,'HR-01-RBI','SE+MON','grid2.irb.hr','','i386','Scientific Linux 3.0.3','Pentium III 1GHz','0.256','0.69','2_4_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (75,'SZTAKI','CE+WN+UI+SE','n31.hpcc.sztaki.hu','','i386','CERN Linux 7.3','2.5 GHz Intel Pentium 4','0.512','0.08','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (76,'SZTAKI','WN','n27.hpcc.sztaki.hu','','i386','CERN Linux 7.3','500 MHz Intel Pentium III Dual','0.256','0.01','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (77,'SZTAKI','WN','n28.hpcc.sztaki.hu','','i386','CERN Linux 7.3','500 MHz Intel Pentium III Dual','0.256','0.01','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (78,'MK01','CE+WN+UI','grid-ce.marnet.net.mk','','i386','CERN Linux 7.3','3 GHz Intel Pentium 4','0.512','0.04','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (79,'MK01','SE','grid-se.marnet.net.mk','','i386','CERN Linux 7.3','3 GHz Intel Pentium 4','0.512','0.04','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (80,'MK01','WN','grid-wn01.marnet.net.mk','','i386','CERN Linux 7.3','3 GHz Intel Pentium 4','0.512','0.04','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (81,'MK01','WN','grid-wn02.marnet.net.mk','','i386','CERN Linux 7.3','3 GHz Intel Pentium 4','0.512','0.04','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (82,'MK01','WN','grid-wn03.marnet.net.mk','','i386','CERN Linux 7.3','3 GHz Intel Pentium 4','0.512','0.04','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (83,'MK01','WN','grid-wn04.marnet.net.mk','','i386','CERN Linux 7.3','3 GHz Intel Pentium 4','0.512','0.04','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (84,'MK02','CE+WN+UI','grid-ce.ii.edu.mk','','i386','CERN Linux 7.3','3.06 GHz Intel Pentium 4 with HT','0.512','0.16','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (85,'MK02','SE+MON','grid-se.ii.edu.mk','','i386','CERN Linux 7.3','800 MHz','0.256','0.04','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (86,'MK02','WN','grid-wn.ii.edu.mk','','i386','CERN Linux 7.3','3.06 GHz Intel Pentium 4 with HT','0.512','0.16','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (87,'MK03','CE+WN+UI','grid-ce.etf.ukim.edu.mk','','i386','CERN Linux 7.3','1.8 GHz Intel Pentium 4','0.512','0.04','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (88,'MK03','SE','grid-se.etf.ukim.edu.mk','','i386','CERN Linux 7.3','1.8 GHz Intel Pentium 4','0.512','0.04','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (89,'MK03','WN','grid-wn.etf.ukim.edu.mk','','i386','CERN Linux 7.3','1.8 GHz Intel Pentium 4','0.512','0.04','LCG2_2_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (90,'RO-01-ICI','CE+WN+BDII','testbed001.grid.ici.ro','','intel i686','SL 3.0.3','2*2.4GHz HT Xeon','1.024','0.12','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (91,'RO-01-ICI','SE+RGMA','testbed002.grid.ici.ro','','intel i686','SL 3.0.3','2.4GHz Celeron','0.512','0.08','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (92,'RO-01-ICI','UI','testbed003.grid.ici.ro','','intel i686','SL 3.0.3','700MHz PIII','0.512','0.08','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (93,'RO-01-ICI','WN','testbed004.grid.ici.ro','','intel i686','SL 3.0.3','2*2.4GHz HT Xeon','1.024','0.12','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (94,'RO-01-ICI','WN','testbed005.grid.ici.ro','','intel i686','SL 3.0.3','2*2.4GHz HT Xeon','1.024','0.12','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (95,'RO-01-ICI','WN','testbed006.grid.ici.ro','','intel i686','SL 3.0.3','2*2.4GHz HT Xeon','1.024','0.12','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (96,'RO-01-ICI','WN','testbed007.grid.ici.ro','','intel i686','SL 3.0.3','2.4GHz Celeron','0.512','0.04','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (97,'RO-01-ICI','WN','testbed008.grid.ici.ro','','intel i686','SL 3.0.3','2.4GHz Celeron','0.512','0.04','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (98,'RoGrid-ICI-GILDA','LCFGNG','student2.ici.ro','','i686','RH7.3','3.0GHz HT','1.024','0.04','LCG2.2');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (99,'RoGrid-ICI-GILDA','SE','student4.ici.ro','','i686','RH7.3','3.0GHz HT','1.024','0.04','LCG2.2');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (100,'RoGrid-ICI-GILDA','CE','student5.ici.ro','','i686','RH7.3','3.0GHz HT','1.024','0.04','LCG2.2');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (101,'RoGrid-ICI-GILDA','WN','student6.ici.ro','','i686','RH7.3','3.0GHz HT','1.024','0.04','LCG2.2');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (102,'RoGrid-ICI-GILDA','UI','student7.ici.ro','','i686','RH7.3','3.0GHz HT','1.024','0.04','LCG2.2');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (103,'RoGrid-INCAS','WN','ce000.incas.ro','','i686','RH 7.3   2.4.x (ROCKS 3.x)','AMD 2100 XP','8.5','1','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (104,'RoGrid-INCAS','CE','ce001.incas.ro','','i686','Scientific Linux 3.03','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (105,'RoGrid-INCAS','UI','ce002.incas.ro','','i686','Scientific Linux 3.03','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (106,'RoGrid-INCAS','SE','ce003.incas.ro','','i686','Scientific Linux 3.03','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (107,'RoGrid-INCAS','WN','ce004.incas.ro','','i686','Scientific Linux 3.03','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (108,'RoGrid-INCAS','PBSClient','ce005.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (109,'RoGrid-INCAS','PBSClient','ce006.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (110,'RoGrid-INCAS','PBSClient','ce007.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (111,'RoGrid-INCAS','PBSClient','ce008.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (112,'RoGrid-INCAS','PBSClient','ce009.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (113,'RoGrid-INCAS','PBSClient','ce0010.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (114,'RoGrid-INCAS','PBSClient','ce011.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (115,'RoGrid-INCAS','PBSClient','ce012.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (116,'RoGrid-INCAS','PBSClient','ce013.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (117,'RoGrid-INCAS','PBSClient','ce014.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (118,'RoGrid-INCAS','PBSClient','ce015.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (119,'RoGrid-INCAS','PBSClient','ce016.incas.ro','','i686','RH 7.3   2.4.x','AMD 2100 XP','0.5','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (120,'RoGrid-NIPNE-01','CE','tbat01.nipne.ro','','intel i686','SL303','2.4GHzPentium 4','0.512','0.25','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (121,'RoGrid-NIPNE-01','SE','tbat02.nipne.ro','','intel i686','SL303','2.8GHz Pentium 4','2.048','0.5','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (122,'RoGrid-NIPNE-01','UI','tbat04.nipne.ro','','intel i686','SL303','2.8GHz Pentium 4','1.024','0.12','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (123,'RoGrid-NIPNE-01','WN','tbat03.nipne.ro','','intel i686','SL303','2.8GHzPentium 4','2.048','0.25','LCG 2_3_1');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (124,'RoGrid-NIPNE-01','WN','tbat05.nipne.ro','','intel i686','SL303','3GHz Pentium 4','2.048','0.12','LCG 2_3_0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (125,'TR-01-ULAKBIM','RB+BDII','ui.ulakbim.gov.tr','','intel \"i386\"','Scientific Linux 3.0.4','Intel P4 3.00Ghz','1','0.12','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (126,'TR-01-ULAKBIM','CE+LocalBDII','ce.ulakbim.gov.tr','','intel \"i386\"','Scientific Linux 3.0.4','Intel P4 3.00Ghz','1','0.12','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (127,'TR-01-ULAKBIM','SE+GridICE','se.ulakbim.gov.tr','','intel \"i386\"','Scientific Linux 3.0.4','Intel P4 3.00Ghz','1','0.24','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (128,'TR-01-ULAKBIM','WN','wn1.ulakbim.gov.tr','','intel \"i386\"','Scientific Linux 3.0.4','Intel P4 3.00Ghz','1','0.12','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (129,'TR-01-ULAKBIM','WN','wn2.ulakbim.gov.tr','','intel \"i386\"','Scientific Linux 3.0.4','Intel P4 3.00Ghz','1','0.12','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (130,'TR-01-ULAKBIM','WN','wn3.ulakbim.gov.tr','','intel \"i386\"','Scientific Linux 3.0.4','Intel P4 3.00Ghz','1','0.12','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (131,'TR-01-ULAKBIM','WN','wn4.ulakbim.gov.tr','','intel \"i386\"','Scientific Linux 3.0.4','Intel P4 3.00Ghz','1','0.12','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (132,'TR-01-ULAKBIM','WN','wn5.ulakbim.gov.tr','','intel \"i386\"','Scientific Linux 3.0.4','Intel P4 2.66Ghz','1','0.8','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (133,'BilkentGrid','UI+RB+BDII','grid1.cs.bilkent.edu.tr','','intel i386','Scientific Linux 3.03','Intel P4-3GhZ','1','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (134,'BilkentGrid','SE','grid2.cs.bilkent.edu.tr','','intel i386','Scientific Linux 3.03','Intel P4-3GhZ','1','0.16','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (135,'BilkentGrid','CE+WN','grid3.cs.bilkent.edu.tr','','intel i386','Scientific Linux 3.03','Intel P4-3GhZ','1','0.08','LCG 2.3.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (136,'TR-03-METU','UI+RB+BDII','ui.ulakbim.gov.tr','','intel \"i386\"','Scientific Linux 3.0.4','Intel P4 3,00Ghz','1','0.08','LCG-2.4.0');
INSERT INTO site_nodes (node_id, site_id, type, machine_name, comments, arch, os, cpu, ram, storage, middleware) VALUES (137,'TR-03-METU','WN','128 nodes behind ui.ulakbim.gov.tr','','intel \"i386\"','UML over Debian','Intel P4 2,66Ghz','128','2.56','LCG-2.4.0');

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS sites;
CREATE TABLE sites (
  country_id varchar(5) NOT NULL default '',
  site_id varchar(20) NOT NULL default '',
  name varchar(100) NOT NULL default '',
  homepage varchar(100) default NULL,
  giis_url varchar(100) default NULL,
  monitoring int(11) default NULL,
  status int(11) default NULL,
  type int(11) default NULL,
  email varchar(30) default NULL,
  phone varchar(30) default NULL,
  emergency_phone varchar(30) default NULL,
  csirt_email varchar(30) default NULL,
  csirt_phone varchar(30) default NULL,
  hours_available varchar(30) default NULL,
  timezone int(11) default NULL,
  OS varchar(30) default NULL,
  middleware varchar(30) default NULL,
  batch_system varchar(30) default NULL,
  VOs_supported varchar(255) default NULL,
  apps_supported varchar(255) default NULL,
  WN_count int(11) default NULL,
  storage_type varchar(30) NOT NULL default '',
  storage_size int(11) default NULL,
  lan_connection float default NULL,
  wan_connection float default NULL,
  comments text,
  admin text NOT NULL,
  timestamp varchar(255) NOT NULL default '',
  PRIMARY KEY  (site_id)
) TYPE=MyISAM;

--
-- Dumping data for table `sites`
--


INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('AL','INIMA','Institute of Informatics and Applied Mathematics','http://grid.inima.al/','ldap://prof.grid.inima.al:2170/mds-vo-name=INIMA,o=grid',1,1,1,'nfra@inima.al','+355 4 22 88 52','+355 4 22 62 34','mbuk@inima.al','+355 4 22 62 34','08:00-17:00',1,'SL 3.0.3','2.3.0','PBS Torque v.1.0.5, Condor Pro','dteam, biomed','',4,'IDE',100,100,1,'','/DC=ORG/DC=SEE-GRID/O=People/O=INIMA/CN=Neki Frasheri','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('BU','BG02-IM','Institute of Mechanics, Bulgarian Academy of Sciences','http://imbm.bas.bg/','',1,1,1,'egee-site-admins@imbm.bas.bg','+359 2 979 2860','+359 2 979 2860','egee-security@imbm.bas.bg','+359 2 979 2860','08:00-17:00',1,'SL 3.0.3.','LCG 2.4.0','PBS Torque v 1.0.5, MPICH','dteam, atlas, lhcb, seegrid, biomed','atlas, lhcb, cdss, gpsa, gptm3d, se4see, vive, SALUTE',0,'IDE',320,100,10,'','/DC=ORG/DC=SEE-GRID/O=People/O=IM-BAS/CN=Kalin Lilovski','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('BU','BG03-IPP-N','Institute for Parallel Processing','http://seegrid.bas.bg/','ldap://ce2.seegrid.bas.bg:2170/mds-vo-name=BG03-IPP-N,o=grid',1,1,1,'seegrid-admin@parallel.bas.bg','+359 2 979 6629','+359 2 979 6793','seegrid-security@parallel.bas.','+359 2 979 6793','08:00-17:00',1,'SL 3.03','LCG 2.4.0','PBS Torque v 1.0.5, maui','dteam, seegrid, biomed','cdss, gpsa, gptm3d, se4see, vive, SALUTE',0,'IDE',150,100,25,'','/DC=ORG/DC=SEE-GRID/O=People/O=IPP-BAS/CN=Ivan Lirkov,/O=GRID-FR/C=BG/O=BAS/OU=IPP/CN=Emanouil Atanassov','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('BU','BG04-ACAD','IPP (Institute for Parallel Processing), ISTF (Information Society Technologies Foundation)','http://grid.acad.bg/','ldap://ce01.grid.acad.bg:2170/mds-vo-name=BG04-ACAD,o=grid',1,1,1,'vgd@acad.bg','+359 2 979 6615','+359 2 979 6614','grid-csirt@acad.bg','+359 2 979 6614','08:00-17:00',1,'SL 3.03','LCG 2.4.0','PBS Torque v 1.0.5, maui','dteam, lhcb, seegrid, biomed','lhcb, cdss, gpsa, gptm3d, se4see, vive, SALUTE',6,'DAS',360,1000,100,'','/DC=ORG/DC=SEE-GRID/O=People/O=IPP-BAS/CN=Luchesar Iliev,/DC=ORG/DC=SEE-GRID/O=People/O=IPP-BAS/CN=Vladimir Dimitrov','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('BU','BG01-IPP','Institute for Parallel Processing, Bulgarian Academy of Sciences','http://grid.bas.bg/','',1,1,1,'egee-admin@parallel.bas.bg','+359 2 979 6793','35929796609','egee-security@parallel.bas.bg','35929796609','08:00-17:00',1,'SL 3.03','LCG 2.4.0','PBS Torque v 1.0.5, maui','dteam, atlas, alice, lhcb, cms, esr, seegrid, biomed','atlas, alice, lhcb, cms, cdss, gpsa, simri3d, gptm3d, esr, se4see, vive, SALUTE',0,'SATA',150,100,25,'','/O=GRID-FR/C=BG/O=BAS/OU=IPP/CN=Emanouil Atanassov','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('SE','AEGIS02-RCUB','Computing Centre, University of Belgrade','http://rcub.bg.ac.yu/','ldap://grid01.rcub.bg.ac.yu:2170/mds-vo-name=AEGIS02-RCUB,o=grid',1,1,1,'ivica@rcub.bg.ac.yu','+381 11 3031257','+381 11 3031257','ivica@rcub.bg.ac.yu','+381 11 3031257','08:00-17:00',1,'SLC 3.0.4','2.4.0','PBS, Condor','dteam, seegrid, biomed','se4see, vive',10,'SAN',1000,100,34,'','','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('SE','AEGIS01-PHY-SCL','Scientific Computing Laboratory, Institute of Physics, Belgrade','http://phy.bg.ac.yu/','ldap://ce.phy.bg.ac.yu:2170/mds-vo-name=AEGIS01-PHY-SCL,o=grid',1,1,1,'antun@phy.bg.ac.yu','+381 11 3160260 Ext 152','+381 11 3160260 Ext 152','antun@phy.bg.ac.yu','+381 11 3160260 Ext 152','08:00-17:00',1,'SL 3.0.4','2.4.0','PBS, Condor','dteam, atlas, cms, seegrid, biomed','atlas, cms, vive, DTEAM, AEGIS VO APPS',8,'IDE',60,100,34,'','/DC=ORG/DC=SEE-GRID/O=People/O=UOB/CN=Antun Balaz','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('GR','test-Grnet','Network Management & Optimal Design Laboratory','http://netmode.ntua.gr/','ldap://grid1.netmode.ece.ntua.gr:2170/mds-vo-name=test-Grnet,o=grid',1,1,1,'amoral@netmode.ntua.gr','302107721449','302107721449','amoral@netmode.ntua.gr','302107721449','08:00-17:00',1,'RH 7.3, SL 3.03','','','dteam, atlas, alice, lhcb, cms, biomed','atlas, alice, lhcb, cms',1,'SE Hard Disk',0,0,100,'','','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('CR','HR-01-RBI','Rudjer Boskovic Institute','http://irb.hr/','ldap://grid1.irb.hr:2170/mds-vo-name=HR-01-RBI,o=grid',1,1,1,'lcg-admin@irb.hr','+387(1)4651111 ext. 1671','+387(1)4651111 ext. 1671','lcg-admin@irb.hr','+387(1)4651111 ext. 1671','08:00-17:00',1,'SL 3.0.3','2_4_0','PBS Torque, Maui','dteam, seegrid, biomed','gpsa, se4see, vive',5,'Disk',690,100,1200,'','/DC=ORG/DC=SEE-GRID/O=People/O=RBI/CN=Valentin Vidic','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('HU','SZTAKI','Computer and Automation Research Institute, Hungarian Academy of Sciences','http://sztaki.hu/','ldap://n31.hpcc.sztaki.hu:2170/mds-vo-name=SZTAKI,o=grid',1,1,1,'patvarc@sztaki.hu','+361 2796066','','','','08:00-17:00',1,'RH 7.3','LCG2_2_0','PBS, Condor,','dteam, biomed','DTEAM',3,'Disk',60,100,100,'','/C=HU/O=KFKI RMKI CA/OU=SZTAKI/CN=Patvarczki Jozsef','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('MA','MK01','MK01','http://marnet.net.mk/','ldap://grid-ce.marnet.net.mk:2170/mds-vo-name=MK01,o=grid',1,1,1,'cano@marnet.net.mk','+38923129068','+38923109324','gone@marnet.net.mk','+38923109324','08:00-17:00',1,'RH 7.3','LCG2_2_0','PBS','dteam, seegrid, biomed','se4see, vive',5,'Disk',240,100,0,'','','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('MA','MK02','MK02','http://ii.edu.mk/','ldap://grid-ce.ii.edu.mk:2170/mds-vo-name=MK02,o=grid',1,1,1,'boroj@ii.edu.mk','+389 70 328 718','+389 70 752 989','goran@','+389 70 752 989','08:00-17:00',1,'RH 7.3','LCG2_2_0','PBS','dteam, seegrid, biomed','se4see, vive',2,'Disk',300,100,34,'','/DC=ORG/DC=SEE-GRID/O=People/O=UKIM/CN=Boro Jakimovski','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('MA','MK03','Macedonian Academic and Research Network (MARNET), University of Sts Cyril and Methodius, Skopje','http://etf.ukim.edu.mk/','ldap://grid-ce.etf.ukim.edu.mk:2170/mds-vo-name=MK03,o=grid',1,1,1,'dan@etf.ukim.edu.mk','+389 70 323 765','','','','08:00-17:00',1,'RH 7.3','LCG2_2_0','PBS','dteam, seegrid, biomed','se4see, vive',2,'Disk',120,100,34,'','','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('RO','RO-01-ICI','National Institute for Research and Development in Informatics, Bucharest','http://grid.ici.ro/','ldap://testbed001.grid.ici.ro:2170/mds-vo-name=RO-01-ICI,o=grid',1,1,1,'alex@ici.ro/atudose@rnc.ro','+40 21 224 12 56','+40 21 224 52 62','alex@ici.ro/atudose@rnc.ro','+40 21 224 52 62','08:00-17:00',1,'SL 3.0.3','LCG 2_3_1','PBS torque','dteam, alice, lhcb, seegrid, biomed','alice, lhcb, gate, cdss, gpsa, simri3d, gptm3d, se4see, vive',6,'local disk',720,100,64,'','/DC=ORG/DC=SEE-GRID/O=People/O=ICI/CN=Alexandru Stanciu','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('RO','RoGrid-ICI-GILDA','RoGrid-ICI-GILDA','http://ici.ro/','ldap://student5.ici.ro:2170/mds-vo-name=RoGrid-ICI-GILDA,o=grid',1,1,1,'vladimir@ici.ro','+40 21 224 07 36 ext. 159','+40 21 224 52 62','vladimir@ici.ro','+40 21 224 52 62','08:00-17:00',1,'RH 7.3','gilda lcg 2.2','PBS openPBS','dteam, biomed','GILDA',1,'local disk',200,100,64,'','','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('RO','RoGrid-INCAS','RoGrid-INCAS','http://incas.ro/','ldap://ce001.incas.ro:2170/mds-vo-name=RoGrid-INCAS,o=grid',1,1,1,'cnae@incas.ro','+40745780140','+40745780140','cnae@incas.ro','+40745780140','08:00-17:00',1,'RH 7.3/9.x, SL 3.0.3','2.3.0','PBS torque, LSF yes, Condor ye','dteam, lhcb, cms, seegrid, biomed','lhcb, cms, gpsa, simri3d, gptm3d, se4see, vive',2,'IDE',1640,100,1,'','','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('RO','RoGrid-NIPNE-01','RoGrid-NIPNE-01','http://nipne.ro/','ldap://tbat01.nipne.ro:2170/mds-vo-name=RoGrid-NIPNE-01,o=grid',1,1,1,'Titi.Preda@nipne.ro/Mihai.Ciub','+40 21 4042361','+40 21 4042361','Titi.Preda@nipne.ro/Mihai.Ciub','+40 21 4042361','08:00-17:00',1,'SL SL303','LCG 2_3_1','PBS torque','dteam, atlas, cms, biomed','atlas, cms',2,'local disk',1240,100,64,'','','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('TU','TR-01-ULAKBIM','Turkish Academic Network and Information Center','http://ulakbim.gov.tr/','ldap://ce.ulakbim.gov.tr:2170/mds-vo-name=TR-01-ULAKBIM,o=grid',1,1,1,'onurt@ulakbim.gov.tr','90 312 298 93 63','90 312 298 93 63','onurt@ulakbim.gov.tr','90 312 298 93 63','08:00-17:00',1,'SL 3.0.4','LCG-2.4.0','PBS torque','dteam, atlas, alice, lhcb, cms, seegrid, biomed','atlas, alice, lhcb, cms, se4see',5,'SATA',200,1000,622,'','/DC=ORG/DC=SEE-GRID/O=People/O=TUBITAK/CN=Onur Temizsoylu','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('TU','BilkentGrid','BilkentGrid','http://cs.bilkent.edu.tr/','ldap://grid3.cs.bilkent.edu.tr:2170/mds-vo-name=BilkentGrid,o=grid',1,1,1,'ekaraca@cs.bilkent.edu.tr','+90 312 2901401','+90 312 2901401','ktayfun@cs.bilkent.edu.tr','+90 312 2901401','08:00-17:00',1,'SL X','','PBS','dteam, seegrid, biomed','se4see',1,'Local',300,100,100,'','/DC=ORG/DC=SEE-GRID/O=People/O=TUBITAK/CN=Evren Karaca','');
INSERT INTO sites (country_id, site_id, name, homepage, giis_url, monitoring, status, type, email, phone, emergency_phone, csirt_email, csirt_phone, hours_available, timezone, OS, middleware, batch_system, VOs_supported, apps_supported, WN_count, storage_type, storage_size, lan_connection, wan_connection, comments, admin, timestamp) VALUES ('TU','TR-03-METU','Middle East Technical University, Computer Center, Ankara','http://ulakbim.gov.tr/','',1,1,1,'onurt@ulakbim.gov.tr','90 312 298 93 63','90 312 298 93 63','onurt@ulakbim.gov.tr','90 312 298 93 63','08:00-17:00',1,'UML over Debian','LCG-2.4.0','Condor','dteam, atlas, alice, lhcb, cms, seegrid, biomed','atlas, alice, lhcb, cms, se4see',1,'PVFS',7000,1000,622,'','/DC=ORG/DC=SEE-GRID/O=People/O=TUBITAK/CN=Onur Temizsoylu','');

--
-- Table structure for table `sitestatus`
--

DROP TABLE IF EXISTS sitestatus;
CREATE TABLE sitestatus (
  status varchar(20) NOT NULL default '',
  PRIMARY KEY  (status)
) TYPE=MyISAM;

--
-- Dumping data for table `sitestatus`
--



--
-- Table structure for table `sitetypes`
--

DROP TABLE IF EXISTS sitetypes;
CREATE TABLE sitetypes (
  type varchar(20) NOT NULL default '',
  PRIMARY KEY  (type)
) TYPE=MyISAM;

--
-- Dumping data for table `sitetypes`
--



--
-- Table structure for table `timezones`
--

DROP TABLE IF EXISTS timezones;
CREATE TABLE timezones (
  timezone_id varchar(10) NOT NULL default '',
  offset int(11) default NULL,
  PRIMARY KEY  (timezone_id)
) TYPE=MyISAM;

--
-- Dumping data for table `timezones`
--



