#!/bin/bash

### get the DB access parameters
. dbaccess.sh

### restore the database 'gocdb' from the backup file
mysql --host=$HOST --user=$USER --password=$PASSWD \
      --database=$DBNAME < gocdb.sql
