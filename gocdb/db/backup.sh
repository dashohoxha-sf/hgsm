#!/bin/bash

### get the DB access parameters
. dbaccess.sh

### dump only the structure of the tables of 'gridrc'
mysqldump --all --add-drop-table --no-data \
          --result-file=gocdb_tables.sql \
          --host=$HOST --user=$USER --password=$PASSWD \
          --databases $DBNAME

### dump both the structure and the data
mysqldump --all --add-drop-table --complete-insert \
          --result-file=gocdb.sql \
          --host=$HOST --user=$USER --password=$PASSWD \
          --databases $DBNAME
