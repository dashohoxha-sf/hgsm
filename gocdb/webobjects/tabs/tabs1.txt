
WebObject 'tabs1' displays a horizontal menu. It is used like this:

    <Include SRC="{{WEBOBJ_PATH}}tabs1.html" />

    <WebObject Class="tabs1" Name="su"
        items="{{./}}su_menu_items.php" />

The attribute 'items' is a PHP file that contains the items of the menu,
like this:
    <?
    /** 
     * The $menu_items array contains the items of the tabs1. 
     */
    $menu_items = array(
            "item1"     => " Menu Item 1 ",
            "item2"     => " Menu Item 2 ",
            "item3"     => " Menu Item 3 ",
            "etc1"      => " . . . ",
            "etc2"      => " . . . "
            );
    ?>

The item of the menu that is selected can be retrieved like this:
    $selected = WebApp::getSVar("tabs1::su->selected_item");

If you need to change the look of the tabs1, then make a local
copy and modify 'tabs1.css'.
